# Write-Up Detailing Simulation C Code:

##### Written by Daniel Rehman on 202308222.122310

## Preface:

The simulation program which is documented in this writeup is aimed at simulating the effect of malicious node behavior on a hetrogeneous network comprised of a limited set of frequency bands which can be transmitted on. Each node on the network has their own distinct usage pattern of when they need to transmit. 

The model used in this code models nodes listening on a band prior to transmitting on it, a simple model of congestion (integer-threshold based) and each node also records internally how trustworthy neighboring node seems to it, based on its behavior. Additionally, nodes adjust their affinity with each other based on their emprical congestion or lack there of.

A Terminology note: Cognitive Radio Secondary users are named "nodes" in this code, and Cognitive Radio Channels or Transmittable Frequency Bands are named "bands". 

## Parameters:

```c
static const double affinity_congestion_penalty = 	0.0002;
static const double trust_congestion_penalty = 		0.0002;

static const double normal_affinity_boost = 		0.0020;    
static const double normal_trust_boost = 		0.0025;   

static const double minimum_acceptable_affinity = 	0.05;
static const double minimum_acceptable_trust = 		0.05;

static const double max_trust = 	 2.0;
static const double max_affinity = 	 2.0;
static const double min_trust = 	-2.0;
static const double min_affinity = 	-2.0;

static const int malicious_probability = 5; 	
static const int minimum_transmit_time = 10;    
static const int congestion_threshold = 3; 	

static const int trial_count = 10;
static const int timestep_count = 2000;

static const int timestep_delay_us = 0;
static const int band_count = 16;
static const int node_count = 40;
```

Here are effectively all the parameters that characterize the simulation. the most important of which are:
- `trial_count` : the number of simulations to run. all simulation CSV output/results are averaged over each simulation.
- `timestep_count` : the number of timesteps, ie amount of time, that each simulation should take.
- `band_count` : the number of frequency bands which nodes can use to transmit on. 
- `node_count` : the number of secondary users on the network, which can transmit on a band. 

Additionally, `timestep_delay_us` provides a way of slowing down the rate at which timesteps happen. setting this value to `1000000`, for example, will make each timestep take 1 second approximately, (as `timestep_delay_us` is in microseconds).

Upon initialization of the system, `malicious_probability` determines the number of nodes out of the total `node_count` which will be selected for being malicious in their behavior. Here, the value `5` means there is a 1 in 5 chance of a node being selected to be malicious. A node being malicious results in it not following the standard protocol/strategy used by other nodes, but instead simply transmitting whenever it's `.usage_pattern` deems is neccessary, regardless of the congestion, or whether it has a low `.trust` or `.affinity` with other nodes on the same band. Ie, malicious nodes only care about transmitting when they want to, not about other nodes behavior. 

Upon the node deciding it should transmit, the `minimum_transmit_time` defines a base-line amount of time that each node will transmit for, once it has found a band to transmit on. 

During a network simulation, if a frequnecy band has *more than* `congestion_threshold` number of nodes actively transmitting on it, the band is deemed "congested". This is the simple integer-threshold model of congestion which nodes use to inform their decisions. for example, in these simulations, 4 nodes transmitting at once consitutes a "congested band" or "congested channel".

`affinity_congestion_penalty` / `trust_congestion_penalty` and `normal_affinity_boost` / `normal_trust_boost` define how fast a node adjusts its `.trust` and `.affinity` values, based on the behavior of its neighbors, and/or congestion. 

Finally, all `.trust` and `.affinity` values are a number between 2.0 and -2.0.


## Data Structures:

```c
enum node_state {
	idle,
	listening,
	transmitting,
};

struct node {
	double* trust;
	double* affinity;
	int malicious;
	int state;
	int time_remaining;
	int usage_pattern;
	int max_transmit_time;
	int on_band;
	double total_time_transmitting;
	double total_time_not_idle;
};

struct band {
	int transmitters[1024];
	int listeners[1024];
	double frequency;
	int transmitter_count;
	int listener_count;
};

```

During a simulation, a node can be in one of 3 states, signified by the value of `.state`: 
- `idle` : the node is choosing to not transmit at this time. 
- `listening` : the node is currently listening to see if the current band which it is listening on, is congested or not. 
- `transmitting` : the node is actively transimttiing on the band, 

A band is defined as having a set `.freqency` (which is not relevant to the simulation at all), and a set of `.transmitters` and `.listeners` which are on the band currently. A hard limit of 1024 is given, but this is adjustable, and not relevant to the simulation. 

A node is defined as having `node_count` number of `.trust` values with all other nodes, including itself, as well as `node_count` number of `.affinity` values. Additionally, a boolean is present telling if the node is `.malicious`, and the `.state`  that the node is currently in, defined above. 

Furthermore, each node *can* have an associated band which it is actively transmitting or listening on currently. when not used, it is set to -1. This is `.on_band`.

Each node's `.usage_pattern` and `.max_transmit_time` is initialized with a random number as follows:

```c
if (not nodes[i].malicious) {
	nodes[i].usage_pattern = rand() % 20 + 4;
	nodes[i].max_transmit_time = rand() % 10 + 4;
} else {
	nodes[i].usage_pattern = rand() % 3 + 4;
	nodes[i].max_transmit_time = rand() % 100 + 4;
}
```

In other words, if a node is chosen to be not malicious, the usage pattern is a number 4 through 23, and 4 through 6 for malicious nodes. Later on, this will be relevant for the node determining when it should transmit. the node simply checks if the expression `rand() % this_nodes.usage_pattern` is 0. if so, the node chooses to transmit. This has the effect that larger `.usage_pattern` values will cause the node to transmit less often, while smaller values will cause it to transmit more often. 

Additionally, the `.max_transmit_time` gives the upper limit for the length of a single transmission. That is, upon a node starting to transmit, it initializes `.time_remaining` to be some random number between the constant `minimum_transmit_time` and the node's own `.max_transmit_time`. this time remaining value ticks down until 0, each timestep the node is transmitting.

`total_time_transmitting`, and `total_time_not_idle` are data which is recorded per node throughout a simulation in order to determine the network's `attr_average_utility` at the end. ignore, if this attribute is not output.


## Recordable Data Fields:

```c
enum recordable_fields {
	none, 
	attr_trust, 
	attr_affinity,
	attr_congested_band_count,
	attr_average_transmitter_count,
	attr_empty_band_count,
	attr_average_mtrust,
	attr_average_trust,
	attr_average_maffinity,
	attr_average_affinity,
	attr_average_utility,
	attr_average_mutility,
	
	total_field_count
};

// ...

static const char* const output_file_name = "output.csv";
static const double congestion_weight = 0.05;

static const int recorded_fields[] = { 
	attr_average_utility, 
	attr_average_mutility, 
}; 
static const int field_count = sizeof(recorded_fields) / sizeof(int);
```

Currently, the utility supports outputting once per timestep certain attributes of nodes during the simulation. these values are averaged over the supplied number of trials. currently, the average utility, and average malicious node utility is being output to a CSV file "output.csv"- 2 data fields per node. 

Note, the `congestion_weight` is to allow a weighted moving average over time of the `congestion_band_count` values, as these can be quite sporadic, averaging helps to smooth the output to see larger patterns. Ignore `congestion_weight` if `attr_congested_band_count` is not outputted.



## Global Variables

```c
static struct node* nodes = NULL;
static struct band* bands = NULL;

static double recorded_values[trial_count][timestep_count][field_count] = {0};
static double average_values[timestep_count][field_count] = {0};
```

This code makes use of the 4 global variables `nodes`, `bands`, `recorded_values`, and `average_values`, and primarily the first two, heavily. this is because the state is managed/accessed by effectively all functions, and thus its mildly pointless to thread the variables through the call stack. 


## Clearing the Screen:

```c
static void clear_screen(void) { printf("\033[H\033[2J"); }
```

Simply uses standard VT-100 escape sequences to clear the terminal screen. This is done each timestep, if `timestep_delay_us` is non-zero. 


## Live Display Functions

The following functions are given for debugging, and visualization purposes to see what is happening within the simulations while actively running them.


```c
static void print_nodes(void) {
	printf("printing state of all nodes: \n");
	for (int i = 0; i < node_count; i++) {

		if (i and not (i % 4)) puts("");
		printf("#%02d:{", i);
		printf(".m=%s, ", nodes[i].malicious ? magenta "bad " reset : "good");
		printf(".p=%02d, ", nodes[i].usage_pattern);
		printf(".u=%2f/%2f, ", nodes[i].total_time_transmitting, nodes[i].total_time_not_idle);
		if (nodes[i].on_band != -1) 
			printf(".b=%02d, ", nodes[i].on_band);
		else 
			printf(".b=  , ");
		
		if (nodes[i].time_remaining > 0) {
			printf(yellow); printf(".t=%02d, ", nodes[i].time_remaining); printf(reset);
		} else {
			printf(".t=  , ");
		}
		printf("s=");

		if (nodes[i].state == transmitting) printf(green);
		printf("[%02u]", i);
		if (nodes[i].state == transmitting) printf(reset);

		printf("}\t\t");
	}
	puts("");
}

static void print_node_values(const int printing_trust) {
	printf("printing trust/affinity of all nodes: \n");
	for (int i = 0; i < node_count; i++) {

		printf(yellow "node #%d" reset ":\t {", i);
		if (printing_trust & 1) printf("." cyan "trust" reset " =    [ ");
		if (printing_trust & 1) for (int j = 0; j < node_count; j++) {
			const double t = nodes[i].trust[j];
			if (t == 0.0) printf(blue);
			if (t < 0.00) printf(red);
			if (t > 0.95) printf(green);
			printf("%5.2lf ", t);
			if (t == 0.0) printf(reset);
			if (t < 0.00) printf(reset);
			if (t > 0.95) printf(reset);
		}

		printf("], \n\t\t  ");
		if (printing_trust & 2) printf("." green "affinity" reset " = [ ");
		if (printing_trust & 2) for (int j = 0; j < node_count; j++) {
			const double a = nodes[i].affinity[j];
			if (a == 0.0) printf(blue);
			if (a < 0.00) printf(red);
			if (a > 0.95) printf(green);
			printf("%5.2lf ", a);
			if (a == 0.0) printf(reset);
			if (a < 0.00) printf(reset);
			if (a > 0.95) printf(reset);
		}
		printf("] } \n");
	}
	printf("\n");
}

```

The `print_nodes` function is used for displaying the current data about all nodes, primarily for debugging purposes, or to see which nodes are malicious. the `.trust` and `.affinity` values are not displayed using this function, rather they have a dedicated function, `print_node_values`. This function displays a matrix of all current `.trust` and `.affinity` values. Note, all nodes are neighbors with all other nodes, and the self-values are hardcoded to be 0.0, and are not changed. 


```c
static void print_ints(int* array, int count) {
	printf("(%d){ ", count);
	for (int i = 0; i < count; i++) {
		printf("%d ", array[i]);
	}
	printf("}");
}
```



This function prints a list of integers, and is used primarily for debugging purposes.


```c
static void print_bands(void) {
	printf("printing bands:\n");
	for (int i = 0; i < band_count; i++) {
		if (bands[i].transmitter_count > congestion_threshold) printf(red);
		printf("\t#%d:{.freq=%05.3fHz,\t", i, bands[i].frequency);

		printf(yellow);
		printf(".listeners = ");
		print_ints(bands[i].listeners, bands[i].listener_count);
		printf(reset);

		printf(bands[i].transmitter_count > congestion_threshold ? red : not bands[i].transmitter_count ? blue : green);
		printf("  .transmitters = ");
		print_ints(bands[i].transmitters, bands[i].transmitter_count);
		printf(reset);

		puts("\n");
	}
	printf("[end of bands]\n");
}
```

This function prints the current state of the bands, printing the index of all nodes in that band, and also color-codes them according to whether they are empty (blue), congested (red), or uncongested (green). additionally, the listeners on each band are also displayed in yellow before each transmitter list. these are normally mostly empty, as nodes do not persist in the listening state for very long. 


## Simulation Functions:


The following functions are used for the process of simulating the network.

```c
static int find_int(int* array, int count, int element) {
	for (int i = 0; i < count; i++) {
		if (array[i] == element) return i;
	}
	abort(); 
}
```

This function `find_int` finds the index of a given integer in a list of integers. if the integer is not found, the entire program halts, as there was a programming error. `element` must exist in the `array` given. 


```c
static void add_listener(const int b, const int node) {
	bands[b].listeners[bands[b].listener_count++] = node;
	nodes[node].on_band = b;
}

static void add_transmitter(const int b, const int node) {
	bands[b].transmitters[bands[b].transmitter_count++] = node;
	nodes[node].on_band = b;
}

static void remove_listener(const int e, const int at, const int i) {
	memmove(bands[e].listeners + at, bands[e].listeners + at + 1, (size_t) (bands[e].listener_count - at - 1) * sizeof(int));
	bands[e].listener_count--;
	nodes[i].on_band = -1;
}

static void remove_transmitter(const int e, const int at, const int i) {
	memmove(bands[e].transmitters + at, bands[e].transmitters + at + 1, (size_t) (bands[e].transmitter_count - at - 1) * sizeof(int));
	bands[e].transmitter_count--;
	nodes[i].on_band = -1;
}
```

The above functions are used to manipulate the bands `.transmitters` and `.listeners` members, in order to move a node from one band to another. each implement a simple array push back, or a simple remove at index algorithm. Additionally, the node's `.on_band` is updated accordingly. 

```c
static double neighbor_affinity(const struct node this, const struct band band) {
	double affinity_with_neighbors = 0.0;
	for (int j = 0; j < band.transmitter_count; j++) {
		affinity_with_neighbors += this.affinity[band.transmitters[j]];
	}
	return affinity_with_neighbors;
}

static double neighbor_trust(const struct node this, const struct band band) {
	double trust_with_neighbors = 0.0;
	for (int j = 0; j < band.transmitter_count; j++) {
		trust_with_neighbors += this.trust[band.transmitters[j]];
	}
	return trust_with_neighbors;
}
```

These two functions `neighbor_affinity` and `neighbor_trust` are used for calculating the total sum of the trust values and affinities of the nodes in the same band as an actively transmitting node. Both are a simple accumulate algorithm.


```c
static void react_to_congestion(int b) {

	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			const double transmitter_i_time_remaining = (double) nodes[transmitter_i].time_remaining;
			const double transmitter_j_time_remaining = (double) nodes[transmitter_j].time_remaining;

			nodes[transmitter_i].affinity[transmitter_j] -= affinity_congestion_penalty * transmitter_j_time_remaining;
			nodes[transmitter_j].affinity[transmitter_i] -= affinity_congestion_penalty * transmitter_i_time_remaining;

			nodes[transmitter_i].trust[transmitter_j] -= trust_congestion_penalty * transmitter_j_time_remaining;
			nodes[transmitter_j].trust[transmitter_i] -= trust_congestion_penalty * transmitter_i_time_remaining;
		}
	}
}
```

A node reacts to congestion on the transmit band it is on, by decreasing the trust and affinity values it has with its neighbors. Once every timestep, if a band is congested, this function is called on it. Here, `b` denotes the band index.

Note that the decrease is proportional to the amount of time the node had left to transmit. Ie, if a node is done transmitting, (`.time_remaining == 0`) then no decrease is made. 

```c
static void nominally_increase_trust(int b) {
	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			nodes[transmitter_i].affinity[transmitter_j] += normal_affinity_boost;
			nodes[transmitter_j].affinity[transmitter_i] += normal_affinity_boost;

			nodes[transmitter_i].trust[transmitter_j] += normal_trust_boost;
			nodes[transmitter_j].trust[transmitter_i] += normal_trust_boost;
		}
	}
}
```

However, if a band is not congested, the affinities and trust values of the nodes transmitting on that band are actually increased instead. they are always increased linearly by the `normal_trust_boost` and `normal_affinity_boost` constant values regardless of the `.time_remaining`.



```c
static void clip_trust_and_affinity(int i) {
	for (int j = 0; j < node_count; j++) {
		nodes[i].trust[j] = nodes[i].trust[j] > max_trust ? max_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] > max_affinity ? max_affinity : nodes[i].affinity[j];

		nodes[i].trust[j] = nodes[i].trust[j] < min_trust ? min_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] < min_affinity ? min_affinity : nodes[i].affinity[j];
	}
}
```

To ensure the trust and affinity values are always in their correct range, they are clipped every timestep by this simple clip algorithm. 

```c
static void tick_clock_for_each_transmitter(int b) {
	for (int u = 0; u < bands[b].transmitter_count; u++) {

		const int transmitter = bands[b].transmitters[u];

		if (nodes[transmitter].time_remaining > 0)  nodes[transmitter].time_remaining--;
		else {
			nodes[transmitter].state = idle;
			nodes[transmitter].time_remaining = -1;
			remove_transmitter(b, u, transmitter); 
			u--;
		}
	}
}
```

Each timestep, for each band, every active transmitter's `.time_remaining` value must be decreased by one. if this results in the value becoming 0, the node is moved into the `idle` state, and removed from the band it was transmitting on. 




```c
static void simulate_timestep(void) {

	for (int b = 0; b < band_count; b++) {
		tick_clock_for_each_transmitter(b);
		if (bands[b].transmitter_count > congestion_threshold) react_to_congestion(b); 
		else nominally_increase_trust(b);
	}

	for (int i = 0; i < node_count; i++) {

		const struct node this = nodes[i];
		const int S = this.state;

		if (S == idle) {
			if (rand() % this.usage_pattern) goto next_node;
		listen_: 
			nodes[i].state = listening;
			add_listener(rand() % band_count, i);
			nodes[i].total_time_not_idle++;


		} else if (S == listening) {
			
			const int on = this.on_band;
			struct band band = bands[on];

			nodes[i].total_time_not_idle++;
			remove_listener(this.on_band, find_int(band.listeners, band.listener_count, i), i);

			
			if (band.transmitter_count > congestion_threshold and not this.malicious) goto listen_;
			

			nodes[i].state = transmitting;
			nodes[i].time_remaining = (rand() % this.max_transmit_time + minimum_transmit_time);
			add_transmitter(on, i);


		} else if (S == transmitting) {
			struct band band = bands[this.on_band];

			nodes[i].total_time_not_idle++;
			nodes[i].total_time_transmitting += 1.0 / (double) bands[this.on_band].transmitter_count;

			if (	this.malicious 							or
				neighbor_trust   (this, band) >= minimum_acceptable_trust      	and 
				neighbor_affinity(this, band) >= minimum_acceptable_affinity) 	goto next_node;

			remove_transmitter(this.on_band, find_int(band.transmitters, band.transmitter_count, i), i);
			goto listen_;
		}

		next_node: clip_trust_and_affinity(i);
	}
}
```

Finally, this leaves us with the most important function: `simulate_timestep`, which calls all of the aforementioned simulation helper functions. 

To begin on each timestep, the bands are updated. this entails simply going over each timestep and either increasing or decreasing trust/affinity values based on whether there is congestion, after decreasing the time remaining values, as shown below:

```c
for (int b = 0; b < band_count; b++) {
	tick_clock_for_each_transmitter(b);
	if (bands[b].transmitter_count > congestion_threshold) react_to_congestion(b); 
	else nominally_increase_trust(b);
}
```

Next, the nodes are updated. depending on which state the node is in, it will be upated differently.

If the node is in the `idle` state, the usage pattern expression given eariler is checked to be zero, and if so, the node is put into the listening state on a random band. This is shown below. 

```c
if (rand() % this.usage_pattern) goto next_node;
listen_: 
	nodes[i].state = listening;
	add_listener(rand() % band_count, i);
	nodes[i].total_time_not_idle++;
```

If the node is in the `listening` state, the node first "listens" on the band by detecting the congestion, and then stops listening on the band and puts itself on a new band if it found congestion. Malicious nodes however, do not pay attention to congestion, and never switch which band they are listening/transmitting on. if the band is not congested, the node is put into the `transmitting` state, and generates a random `.time_remaining` value to tell how long to transit, and the node marks itself as a transmitter on the band it was just listening on. This is show below.

```c
const int on = this.on_band;
struct band band = bands[on];

nodes[i].total_time_not_idle++;
remove_listener(this.on_band, find_int(band.listeners, band.listener_count, i), i);


if (band.transmitter_count > congestion_threshold and not this.malicious) goto listen_;


nodes[i].state = transmitting;
nodes[i].time_remaining = (rand() % this.max_transmit_time + minimum_transmit_time);
add_transmitter(on, i);
```


if the node is in the `transmitting` state, first the data about how much congestion it is experiencing is recorded in `.total_time_transmitting`, and then the node checks to see if it should switch to another band based on whether its neighbors have a high affinity with them, and are deemed "trustworthy". the node persists in the `transmitting` state until the `tick_clock_for_each_transmitter()` function takes it off the band as its timer expired. if the trust/affinity values drop below the acceptable amount while transmitting before this happens, the node removes itself from the band, and returns to the `listening` state to find another band to transmit on. This is show below.

```c
struct band band = bands[this.on_band];

nodes[i].total_time_not_idle++;
nodes[i].total_time_transmitting += 1.0 / (double) bands[this.on_band].transmitter_count;

if (	this.malicious 							or
	neighbor_trust   (this, band) >= minimum_acceptable_trust      	and 
	neighbor_affinity(this, band) >= minimum_acceptable_affinity) 	goto next_node;

remove_transmitter(this.on_band, find_int(band.transmitters, band.transmitter_count, i), i);
goto listen_;
```

Finally, after each node, the trust and affinity values are clipped, as shown below:

```c
next_node: clip_trust_and_affinity(i);
```




## Interrupt Handler : Dyamic Displaying of Simulation State 



```c
static volatile bool running = true;
static volatile bool should_print_nodes = false;
static volatile bool should_print_bands = false;
static volatile int  should_print_values = 0;

static void handler(int __attribute__((unused))_) {

loop:	printf("entered interrupt handler:(q/b/n/t/a) ");
	fflush(stdout);
	const int c = getchar(); getchar();

	if (c == 'q') exit(0);

	else if (c == 'h') running = not running;
	else if (c == 'n') should_print_nodes = not should_print_nodes;
	else if (c == 'b') should_print_bands = not should_print_bands;

	else if (c == 't') should_print_values ^= 1;
	else if (c == 'a') should_print_values ^= 2;
	else {
		printf("unknown interrupt command: %c\n", c);
		goto loop;
	}
}
```


This section of the code allows the utility to change what information is displayed during the simulation, dynamically according to the users commands. By default, nothing is displayed about the current simulation state. if the user presses Ctrl-C, the interrupt handler triggers and allows the user to type a character to toggle certain display elements. These are outlined below:

- `q` : This quits the utility instantly. Nothing is saved.
- `b` : This toggles displaying the band information via `print_bands()`.
- `n` : This toggles displaying the node information via `print_nodes()`.
- `t` : This toggles displaying the trust values for each node, via `print_node_values(should_print_values)`.
- `a` : This toggles displaying the affinity values for each node, via `print_node_values(should_print_values)`. 


Both trust and affinity can be visualized at the same time, as `t` and `a` are both simply a bit toggle in the value `should_print_values`, and are fully independent. 






## Simulation Data Collection and Data Outputting 



```c
static void compute_average_trust_affinity(
	double* out_mtrust, 
	double* out_trust,
	double* out_maff,
	double* out_aff
) {
	int mcount = 0;
	int count = 0;
	int fmcount = 0;
	int fcount = 0;
	double mtrust = 0.0;
	double trust = 0.0;
	double maff = 0.0;
	double aff = 0.0;

	for (int i = 0; i < node_count; i++) {
		if (nodes[i].malicious) continue;

		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				mtrust += nodes[i].trust[j];
				mcount++;
			} else {
				trust += nodes[i].trust[j];
				count++;
			}
		}
		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				maff += nodes[i].affinity[j];
				fmcount++;
			} else {
				aff += nodes[i].affinity[j];
				fcount++;
			}
		}
	}
	*out_mtrust = mtrust / mcount;
	*out_trust = trust / count;
	*out_maff = maff / fmcount;
	*out_aff = aff / fcount;
}
```

This function `compute_average_trust_affinity` computes the average trust value between all non-malicious (N) and non-malicious (N) nodes, put in `out_trust`, and the average trust value between all non-malicious (N) and malicious (M) nodes, given by `out_mtrust`. 

Additionally, this function uses exactly the same algorithm for finding the average affininity between N-N and N-M nodes. 


```c
static void record_data(const int trial, const int ts) {
	if (not field_count) return;

	static double congestion = 0;
	static double emptiness = 0;

	for (int field = 0; field < field_count; field++) {

		const int F = recorded_fields[field];

		double average_transmitter_count = 0, average_utility = 0, average_mutility = 0, 
			mtrust = 0, trust = 0, maff = 0, aff = 0;

		compute_average_trust_affinity(&mtrust, &trust, &maff, &aff);

		if (F == attr_average_utility) {

			int count = 0;
			for (int i = 0; i < node_count; i++) {
				if (nodes[i].malicious) continue;
				if (not nodes[i].total_time_not_idle) continue;
				const double ratio = nodes[i].total_time_transmitting / (double) nodes[i].total_time_not_idle;
				average_utility += ratio;
				count++;
			}

			if (count) average_utility /= count;
			else average_utility = 0;
		}

		if (F == attr_average_mutility) {

			int count = 0;
			for (int i = 0; i < node_count; i++) {
				if (not nodes[i].malicious) continue;
				if (not nodes[i].total_time_not_idle) continue;
				const double ratio = nodes[i].total_time_transmitting / (double) nodes[i].total_time_not_idle;
				average_mutility += ratio;
				count++;
			}

			if (count) average_mutility /= count;
			else average_mutility = 0;
		}

		if (F == attr_congested_band_count) {
			double this_congestion = 0;
			for (int b = 0; b < band_count; b++) 
				if (bands[b].transmitter_count > congestion_threshold) 
					this_congestion++;

			if (not congestion) congestion = this_congestion;
			else congestion = (1.0 - congestion_weight) * congestion + congestion_weight * this_congestion; 
		}


		if (F == attr_empty_band_count) {
			double this_emptiness = 0;
			for (int b = 0; b < band_count; b++) 
				if (not bands[b].transmitter_count) this_emptiness++;

			if (not emptiness) emptiness = this_emptiness;
			else emptiness = (1.0 - congestion_weight) * emptiness + congestion_weight * this_emptiness; 
		}

		if (F == attr_average_transmitter_count) {
			double sum = 0;
			for (int b = 0; b < band_count; b++) sum += bands[b].transmitter_count;
			average_transmitter_count = sum / (double) band_count;
		}

		double value = 0;
		if (F == attr_congested_band_count) 		value = congestion;
		if (F == attr_empty_band_count) 		value = emptiness;
		if (F == attr_average_transmitter_count) 	value = average_transmitter_count;
		if (F == attr_average_mtrust) 			value = mtrust;
		if (F == attr_average_trust) 			value = trust;
		if (F == attr_average_maffinity) 		value = maff;
		if (F == attr_average_affinity) 		value = aff;
		if (F == attr_average_utility) 			value = average_utility;
		if (F == attr_average_mutility) 		value = average_mutility;

		recorded_values[trial][ts][field] = value;
	}
}
```

The `record_data()` function is responsible for caching to the `recorded_values` array the different fields which are desired to be recorded across all simulations. for each field, a case statement calculates the relevant value and assigns it to its corresponding spot in the `recorded_values` array, `recorded_values[trial][ts][field] = value;`.



The `attr_average_utility` and `attr_average_mutility` functions use a similar algorithm for simply averaging the ratio `nodes[i].total_time_transmitting / (double) nodes[i].total_time_not_idle;` for each node. this ratio is deemed to be proportional or representative to the "utility" or "reward" that each node recieves while transmitting, as the `total_time_not_idle` variable is incremented every time the node is not in the `idle` state, and the `total_time_transmitting` variable is updated by `(1.0 / bands[.on_band].transmitter_count)` which causes the "utility" value to be lower if there are several other transmitters simultaneously transmitting at the time of this node transmitting. 

```c
static void compute_average_data(void) {
	for (int ts = 0; ts < timestep_count; ts++) {
		for (int field = 0; field < field_count; field++) {
			double sum = 0;
			for (int trial = 0; trial < trial_count; trial++)   
				sum += recorded_values[trial][ts][field];
			average_values[ts][field] = sum / trial_count;
		}
	}
}

```

This function `compute_average_data` is run at the end of all trials, and looks over all timesteps of all trials, and averages the values recorded across the `trial_count` trials. 


```c
static void write_average_data(void) {
	if (not field_count) return;
	printf("writing the recorded data [%d, %d]...\n", field_count, timestep_count);

	FILE* file = fopen(output_file_name, "w+");
	if (not file) { perror("fopen"); exit(1); }

	fprintf(file, "TS(%d.%d.%d.%d), ", trial_count, timestep_count, node_count, band_count);
	for (int field = 0; field < field_count; field++)  {
		fprintf(file, "%s", recordable_field_spelling[recorded_fields[field]]);
		if (field < field_count - 1) fprintf(file, ", ");
	}
	fprintf(file, "\n");

	for (size_t ts = 0; ts < timestep_count; ts++) {
		fprintf(file, "%lu,", ts);
		for (int field = 0; field < field_count; field++)  {
			fprintf(file, "%10.5lf", average_values[ts][field]);
			if (field < field_count - 1) fprintf(file, ",");
		}
		fprintf(file, "\n");
	}

	fclose(file);
}
```

This function writes the recorded and trial-averaged data to a .CSV file. each field is a column in the .CSV file.






## Initialization:




```c
static void initialize_nodes(void) {

	for (int i = 0; i < node_count; i++) {
		nodes[i].trust = calloc(node_count, sizeof(double));
		nodes[i].affinity = calloc(node_count, sizeof(double));
	}
	
	for (int i = 0; i < node_count; i++) {

		nodes[i].malicious = not (rand() % malicious_probability);
		nodes[i].state = idle;
		nodes[i].time_remaining = -1;
		nodes[i].on_band = -1;

		if (not nodes[i].malicious) {
			nodes[i].usage_pattern = rand() % 20 + 4;
			nodes[i].max_transmit_time = rand() % 10 + 4;
		} else {
			nodes[i].usage_pattern = rand() % 3 + 4;
			nodes[i].max_transmit_time = rand() % 100 + 4;
		}

		for (int j = 0; j < node_count; j++) {

			nodes[i].trust[j] =    (double)(rand() % 10000) / (double) 10000;
			nodes[i].affinity[j] = (double)(rand() % 10000) / (double) 10000;

			if (i == j) { nodes[j].affinity[i] = 0.00; }
			if (i == j) { nodes[j].trust[i]    = 0.00; }
		}
	}
}
```

to initialize the nodes to prepare for a new trial, first all trust and affinity values are allocated and initialized to zero. then, the `.malicious` field is populated based on the `malicious_probability`, and all nodes are put into the `idle` state, not on any band yet. 

Then the `.usage_pattern` and `.max_transmit_time` values are populated as explained previously, and finally, the trust and affinity values are initialized with a random floating point value between 0.0 and 1.0, except for self-trust or self-affinity values, which are given the value 0.0 always.


```c
static void initialize_bands(void) {
	for (int b = 0; b < band_count; b++) {
		bands[b].frequency = (double) (rand() % 45 + 45);
		bands[b].frequency *= 10;
	}
}
```

For initializing the bands, simply a random freqency is generated for each band (whose value is irrelevant and not used). All other data is already initialized to zero when `band` array is allocated.


## Main Function:

```c
int main(void) {

	srand((unsigned) time(0));
	struct sigaction action = {.sa_handler = handler}; 
	sigaction(SIGINT, &action, NULL);

	for (int trial = 0; trial < trial_count; trial++) {

		printf("------------------- trial %d ------------------\n", trial);

		bands = calloc(band_count, sizeof(struct band));
		nodes = calloc(node_count, sizeof(struct node));

		initialize_bands();
		initialize_nodes();

		puts("simulating network...");

		for (int ts = 0; ts < timestep_count and running; ts++) {

			if (timestep_delay_us) clear_screen();

			if (should_print_values) 	print_node_values(should_print_values);
			if (should_print_nodes) 	print_nodes();
			if (should_print_bands) 	print_bands();
			
			simulate_timestep();
			
			if (timestep_delay_us) { fflush(stdout); usleep(timestep_delay_us); }

			record_data(trial, ts);
		}

		double mtrust = 0.0, trust = 0.0, maff = 0.0, aff = 0.0;

		compute_average_trust_affinity(&mtrust, &trust, &maff, &aff);

		printf("mtrust = %lf, trust = %lf\n", mtrust, trust);
		printf("maffinity = %lf, affinity = %lf\n", maff, aff);

		free(nodes);
		free(bands);
	}

	compute_average_data();
	write_average_data();
}
```

To begin, the interrupt handler is registered, and the random number generator is seeded based on the time. for each trial, the following is done:

 - the nodes and bands are completely reinitialized
 - the network is simulated (see next list)
 - information about the average trust and affinity is given as a helpful piece of information for debugging.

To simulate the network once, the following is done:

 - the screen is cleared, if the user has set `timestep_delay_us` to be nonzero.
 - the relevant information is displayed about the current state of the simulation, based on the users interrupt requests.
 - the timestep is simulated via `simulate_timestep()`.
 - the screen is flushed to prevent flickering, and `usleep(timestep_delay_us)` is called if `timestep_delay_us` is nonzero.
 - the data for the current timestep and trial is recorded via `record_data(trial, ts)`.

Once all trials are complete the program computes the averaged data via `compute_average_data()` and writes this average data to an output CSV file via `write_average_data()`. 

Finally, the program implicitly returns the exit code `0`.






