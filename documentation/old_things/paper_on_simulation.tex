\documentclass[sigconf]{acmart}
\usepackage{float}
\usepackage{listings}
\usepackage{color}
\usepackage{amsmath}

\definecolor{dkgreen}{rgb}{0,0.6,0}
\definecolor{gray}{rgb}{0.5,0.5,0.5}
\definecolor{mauve}{rgb}{0.58,0,0.82}
\definecolor{commentsColor}{rgb}{0.58,0,0.82}

\lstdefinelanguage{JavaScript}{
  keywords={typeof, new, async, execute, catch, function, return, null, catch, switch, let, if, in, while, do, else, case, break},
  keywordstyle=\color{blue}\bfseries,
  ndkeywords={class, export, boolean, throw, implements, import, this},
  ndkeywordstyle=\color{darkgray}\bfseries,
  identifierstyle=\color{black},
  sensitive=false,
  comment=[l]{//},
  morecomment=[s]{/*}{*/},
  commentstyle=\color{purple}\ttfamily,
  stringstyle=\color{red}\ttfamily,
  morestring=[b]',
  morestring=[b]"
}

\lstset{
  backgroundcolor=\color{white},
  basicstyle=\footnotesize,     
  breakatwhitespace=false,      
  breaklines=true,              
  captionpos=b,                 
  commentstyle=\color{commentsColor}\textit,  
  deletekeywords={...},        
  escapeinside={\%*}{*)},      
  extendedchars=true,          
  frame=tb,	               
  keepspaces=true,           
  keywordstyle=\color{keywordsColor}\bfseries,    
  language=JavaScript,          
  otherkeywords={*,...},        
  numbers=left,                 
  numbersep=2pt,                
  numberstyle=\tiny\color{commentsColor},
  rulecolor=\color{black},    
  showspaces=false,           
  showstringspaces=false,     
  showtabs=false,             
  stepnumber=1,               
  stringstyle=\color{stringColor},
  tabsize=2,	             
  title=\lstname,            
  columns=fixed              
}

\AtBeginDocument{%
  \providecommand\BibTeX{{%
    \normalfont B\kern-0.5em{\scshape i\kern-0.25em b}\kern-0.8em\TeX}}}

\begin{document}

\title{Current State and Architecture of Simulation Code}

\author{Daniel Rehman}
\email{daniel.rehman@wsu.edu}
\affiliation{%
  \institution{Washington State University}
  \city{Vancouver}
  \state{Washington}
  \country{USA}
  \postcode{98665}
}

\renewcommand{\shortauthors}{Rehman}
\maketitle

\section{Introduction}

The simulation program which is documented in this write-up is aimed at simulating the effect of malicious node behavior on a heterogeneous network comprised of a limited set of frequency bands which can be transmitted on. Each node on the network has their own distinct usage pattern of when they need to transmit, and the hope is that nodes will self partition themselves into groups which are able to share the same band with each other with minimal congestion.

The model used in this code models nodes listening on a band prior to transmitting on it, a simple model of congestion (integer-threshold based) and each node also records internally how trustworthy neighboring node seems to it, based on its behavior. Additionally, nodes adjust their affinity with each other based on their empirical congestion or lack there of.

A Terminology note: Cognitive Radio Secondary users are named "nodes" in this code, and Cognitive Radio Channels or Transmittable Frequency Bands are named "frequency bands", or just "bands".


\subsection{General Outline and Key Parameters}


A predefined number of independent instances of the cognitive radio network simulation are run. Each of these instances is refered to as a "trial". each of these instances is a network simulation which simulates the networks behavior for a certain number of timesteps. The number of secondary user nodes and frequency bands available for transmitting can drastically change the resultant behavior of the network. 

Thus, these are the key parameters which characterize the simulation:

\begin{itemize}
  \item Trial count  : the number of simulation instances to run. all simulation CSV output/results are averaged over each simulation.
  \item Timestep count  : the number of timesteps, ie amount of time, that each simulation should take.
  \item Band count  : the number of frequency bands which nodes can use to transmit on. 
  \item Node count  : the number of secondary users on the network, which can transmit on a band. 
\end{itemize}

While simulating the network for any particular simulation instance, Data is recorded about the networks behavior each timestep, and this data is eventually written to a file. for more information, see section "Data Collection".

It should be noted that the written data is averaged across each instance of the simulation. That is, data for timestep \#1 of instance \#1 is averaged with data for timestep \#1 of instance \#2, \#3, etc. This process ensures that there any statistical randomness or noise resulting in a spike in the data in one instance of the simulation will be averaged out over all instances of the simulations, leaving only the statistically significant parts of the data present, ideally.


\section{Initialization of Simulation}

When initializing the network state prior to simulating it, Each node has a certain prior probability of being a maclious node. Whether a node is malicious is chosen before the simulation starts. Each node is in the idle state, and is assigned a particular "usage pattern", which tells the node how often and when to start transmitting/listening on a frequency band. This usage pattern is used for causing a node to transition from the idle state to the listening state. See section Simulation Logic for more information.

Each node also has a trust and affinity value, which is a floating point number between 2.0 and -2.0. at the start of the simulation, all nodes have a random floating-point trust and affinity value with all other nodes except itself. Each of these initial trust/affinity values is within 0.0 and 1.0 at the start of the simulation. 

Finally, all frequency bands start out as empty, having no active transmitters or listeners. 




\section{Simulation Logic}

	on each timestep, code is done for updating the frequency band's data is done, followed by code for updating the node's data. 


\subsection{Band Update logic}

To update the frequency bands, the following steps are done. To start, for each band the current nodes who are transmitting on that band are examined to see if they have "finished transmitting" yet. For each transmitting node on the frequency band, the "transmission time remaining" of the node is decreased by one (implicitly, one timestep). If the transmission time remaining for the node reaches zero, the node is removed from the frequency band, and put back into the idle state. 

The band update function can be formulated as the following mathematical function:

    $$ tick_{t + 1}() = \begin{cases} 
      tr_{t + 1} = tr_{t} - 1   & ... tr_{t} > 0 \\
      s = 0, tr_{t + 1} = -1    & ... tr_{t} \leq 0 \\
    \end{cases} $$

Where $tr$ is the time remaining for the transmission of the given node., $s$ is the state of the given node, and $t$ is the current timestep. Next, each band is examined to see if it is congested. congestion is defined as a simple threshold (defined in the simulation parameters) on the number of active transmitters. 

If the band is not congested, the trust and affinity values between every possible pair of transmitters on that band are adjusted by adding a small floating-point amount to the trust and affinity value. in other words, transmitters which are transmitting together nominally have their trust/affinity values grow with each other, and thus "trust" each other more and more as time goes on, as long as they are transmitting together with no congestion. 

On the other hand if the band is congested, then the trust and affinity value between every possible pair of transmitters on that band are decreased by a more significant amount. this happens every timestep while the band is still congested. this is meant to try to encourage the non-malicious nodes to leave the band, and find a new band to transmit on.

Note, it is ensured that the trust/affinity value between node X and node Y, and the trust/affinity value between node Y and node X are adjusted in the exact same amount, to keep these values synchronized. 

Note that the trust/affinty value of a node with itself is not adjusted in any situation. 


\subsection{Node update logic}

To update the nodes, first the state of each node is examined. A certain action is taken on the nodes values based on the node's state.

A node can be in one of three states:

\begin{itemize}
  \item Idle  : the node is choosing to not transmit or listen at this time. 
  \item Listening  : the node is currently listening to see if the current band which it is listening on, is congested or not. 
  \item Transmitting  : the node is actively transimttiing on the band, 
\end{itemize}

The node update/transition function can be formulated as the following mathematical function:

    $$ simulate_{n,(t + 1)}(s,b) = \begin{cases} 
      idle() & ... s = 0 \\
      listening() & ... s = 1 \\
      transmitting() & ... s = 2 \\
    \end{cases} $$

Where $s$ is the state of the given node which is simulated, and $b$ is the band which the node is currently on. 


\subsubsection{Idle State}

If the nodes state is idle, then the node waits to initiate transmitting or listenening until its usage pattern directs it to start transmitting/listening. once the usage pattern has indicated this, the node puts itself into the listening state, and puts itself as a listener on a random frequency band. 

The node idle state manipulation function is given as:
   
    $$ idle() = \begin{cases} 
      do.nothing                & ... R() \mod U \neq 0 \\
      s = 1, b = R() \mod bc    & ... R() \mod U = 0
    \end{cases} $$ 

Where R() is a pseudo-random number generator, which returns a random integer between $2^32$ and $0$, and $s$ is the nodes state, $bc$ is the number of bands, and $U$ is the integer-valued usage pattern for the node. 


\subsubsection{Listening State}

If the nodes state is listening, then the node checks to see if the band it is listening on is congested (ie, the number of active transmitters on that band is strictly greater than some threshold, defined in the simulation parameters). if the band is congested, the node removes itself as a listener from that band, and randomly picks a new band in order to start listening on. the node remains in the listening state if this occurs. if the band is not congested, then node decides to stop listening on that band, and start actively transmitting on it, for a random amount of time between the minimum and maximum allowed transmit times.  

The node listening state manipulation function is given as:
   
     $$ listening() = \begin{cases} 
        s = 1, b = R() \mod bc                  & ... b.tc > ct \\
        s = 2, tr_{t + 1} = m + (R() \mod M)    & ... b.tc \leq ct
    \end{cases} $$ 

Where $tr$ is the transmission time remaining for a node, and $s$ is the nodes state, and $M$ is the maximum transmission time and $m$ is the minimum transmission time, and $b.tc$ is the current bands transmitter count, and $ct$ is the congestion threshold parameter. 

In all the simulations ran, $ct$ is equal to $3$. That is, if a given node is on a band which has three or more other nodes also on that band, then the given node will consider the band it is on as "congested", and will take action accordingly. In the Listening state, this ends up resulting in the node switching to listening on a different band. if the band is not "congested" by this definition, the node chooses to transmit on the band for a set amount of time given by $tr$, transmission time remaining. 

Note, malicious nodes in the listening state ignore this conditionality entirely: a malicious node in the listening state will always proceed to transmit on the band it chose to listen on. 


\subsubsection{Transmitting State}

If the nodes state is transmitting, the first node checks whether it should continue to transmit on this particular frequency band. it does this by accumulating the trust values it has with all other active transmitters on this band, and if this sum falls below a certain threshold (defined in the simulation parameters), then the node chooses to stop transmitting on this band, and start listening on a new band. the node puts itself in the listening state if this happens. 

If the sum does not fall below this threshold, the node simply continues transmitting on this band until its transmission time remaining reaches zero, in which it will be automatically removed from the band and put back into the idle state. 

The node transmitting state manipulation function is given as:

    $$ transmitting() =  \begin{cases} 
      do.nothing      & ... K_{T} > K_{T.t} \land K_{a} > K_{a.t} \\
      s = 1, b = -1   & ... otherwise
    \end{cases} $$ 

Where $K_{T}$ is the neighborhood summation of the trust values, $K_{T.t}$ is the corresponding threshold, $K_{a}$ is the neighborhood summation of the affinity values, and $K_{a.t}$ is the corresponding threshold, and $b$ is the band the given node is currently transmitting on, and $s$ is the nodes state. 

Note that two of these checks take place: one for the trust values, which was just described, and a corresponding one for the affinity values. there are two seperate thresholds for the neighborhood trust sum, and the neighborhood affinity sum.

Note that malicious nodes in the transmitting state ignore this check for evacuating a frequency band entirely: a malicious node will always transmit continuously with no interruptions on the band it has chosen. 


\section{Data Collection}

After the execution of all simulation instances has completed, the trial-averaged version of the data that has been marked for collection is written to a CSV file. how each datum is collected/generated is explained below.

Many quanitative attributes of the behavior simulation over time can be selected for data collection. the simplest of these include: the number of empty bands for a particular timestep, the average number of transmitters on a band for a timestep, and a moving average across timesteps of the numger of congested bands in the network, the average trust or affinity values between nodes. These need little explanation. the code which collects this data involves only a simple counter recording the number of items of that type, and an average calculation if applicable.

Other quantative attributes of the behavior of the simulation that can be selected include: the average utility of the network, and the average band usage of the network. 
		
The average utility ("reward") is calculated via the Shannon-Hartley Theorem, which is given below.


	$$ R(K) = B * log(1 + \frac{P_s * G_s}{\sum_{i=1}^{K}P_i * G_{si}} + W) $$


Where $B$ is the channel bandwidth, $G_s$ and $G_{si}$ are the channel gains for the transmissions of s, $P_i$ is the transmission power, and $W$ is the additive Gaussian white background noise.

The average band usage of the network is given by the formula given below, and has the goal of characterizing the ratio that nodes spend in the idle/listening state, rather than the transmitting state, while also taking into account how many other transmitters were present on the band at the time of transmitting. 


	$$ U(L, T) = \frac{1}{L} * \sum_{t=0}^{T} \frac{1}{K_t} $$


Where $T$ is the number of timesteps, $L$ is the total time spent not in the idle state, and $K_t$ is the number of other active transmitters on the same band as the given node on timestep $t$.



\bibliographystyle{ACM-Reference-Format}
\bibliography{images}
\end{document}
\endinput




































\iffalse



% world(t, y_t) 
% \frac{100-x}{100}






\section {Appendix: Mathematical Variables and Corresponding Software Names}


The software name for the variables used in the above formulas are given below:

    $$ s = state $$
    $$ b = on.band $$
    $$ bc = band.count $$
    $$ tc = band.transmitter.count $$
    $$ tr = time.remaining $$
    $$ M = max.transmit.time $$
    $$ U = usage.pattern $$
    $$ m = minimum.transmit.time $$
    $$ T = total.time.transmitting $$
    $$ T = total.time.transmitting $$






\section{Operating The Simulation Software}

The simulation parameters must be chosen prior to running the simulation. To set these, simply edit the set of constants listed at the top of the "simulation.c" source file. Once the parameters are chosen, the simulation program can be used in interactive mode, or data generation mode. By default, data generation mode is used. 

Note that data is still generated in interactive mode, and internally data generation mode is simply interactive mode with no information selected to be displayed on each timestep. 

In interactive mode, control-C followed by a character in the set $\{q,h,n,b,t,a\}$ can be used to toggle a certain attribute of the simulatin to be displayed each timestep, in order to see what is happening in a particular simulation instance over time. the meaning of these characters is given below.

\begin{itemize}
	\item 'q'  : Quit the utility. causes the process to exit immediately. 
	\item 'h'  : Halt the utility. stops the simulation gracefully, data is still generated. 
	\item 'n'  : Print data about each node. 
	\item 'b'  : Print data about which node is on which band
	\item 't'  : Print the current trust values for all nodes
	\item 'a'  : Print the current affinity values for all nodes
\end{itemize}

\fi