#include <stdio.h>     // written by dwrr on 202307311.202047: thesis research:
#include <stdlib.h>    // simulation of cognitive radio secondary transmitters on a network containing rogue nodes,
#include <string.h>    // trying to develop an algorithm/model which can detect rogue nodes using a trust model.
#include <unistd.h>
#include <iso646.h>
#include <stdbool.h>
#include <time.h>
#include <signal.h>

/*
todo:
	- visualize how nodes group together with others-  groups in the neighborhood graph using  affinity values!!!

	- differentiate trust and affinity more...


	- redo the simulation to account for    LISTENING of a node.  (not always transmitting!)


*/

#define red   	"\x1B[31m"
#define green   "\x1B[32m"
#define yellow  "\x1B[33m"
#define blue   	"\x1B[34m"
#define magenta "\x1B[35m"
#define cyan   	"\x1B[36m"
// #define bold    "\033[1m"
#define reset 	"\x1B[0m"

enum node_state {
	idle,
	listening,
	transmitting,
};

enum recordable_data_fields {
	none, 
	attr_trust, 
	attr_affinity,
	attr_congested_band_count,
	attr_average_transmitter_count,
	attr_empty_band_count,
	attr_average_mtrust,
	attr_average_trust,
	
	total_field_count
};

static const char* recordable_data_field_spelling[total_field_count] = {
	"None", 
	"Trust", 
	"Affinity",
	"Congested_Band_Count",
	"Average_User_Count",
	"Empty_Band_Count",
	"Average_N-M_Trust",
	"Average_N-N_Trust",
};


struct node {
	double* trust;
	double* affinity;
	int malicious;
	int state;
	int time_remaining;
	int usage_pattern;
	int max_transmit_time;
	int on_band;
	int total_time_transmitting;
	int total_time_not_idle;
};

struct band {
	int transmitters[1024];
	int listeners[1024];
	double frequency;
	int transmitter_count;
	int listener_count;
};


/// ------------------  parameters:  ------------------ 

// static const bool debug = false;



static const double affinity_congestion_penalty = 	0.0002;
static const double trust_congestion_penalty = 		0.0002;

static const double normal_affinity_boost = 		0.0020;        // how fast 
static const double normal_trust_boost = 		0.0025;        // how fast nodes come to trust each other.

static const double minimum_acceptable_affinity = 	0.05;
static const double minimum_acceptable_trust = 		0.05;

static const double max_trust = 	 2.0;
static const double max_affinity = 	 2.0;
static const double min_trust = 	-2.0;
static const double min_affinity = 	-2.0;



static const int malicious_probability = 5; 		// nodes have a (1 / X) chance of being malicious.
static const int minimum_transmit_time = 10;            // baseline amount of time to transmit on the network.
static const int congestion_threshold = 3; 		// number of nodes to consider a band/channel as congested.




static const int trial_count = 10;

static const int timestep_count = 10000;		//1000;


static const int timestep_delay_us = 0;		//0;


static const int band_count = 16;
static const int node_count = 40;




static const char* const output_file_name = "output.csv";

static const double congestion_weight = 0.05;
static const int recorded_data_fields[] = { attr_average_mtrust, attr_average_trust }; 
static const int field_count = sizeof(recorded_data_fields) / sizeof(int);


/// --------------------------------------------------- 



static struct node* nodes = NULL;
static struct band* bands = NULL;

static double recorded_values[trial_count][timestep_count][field_count] = {0};
static double average_values[timestep_count][field_count] = {0};


static void clear_screen(void) { printf("\033[H\033[2J"); }


static void print_nodes(void) {
	printf("printing state of all nodes: \n");
	for (int i = 0; i < node_count; i++) {

		if (i and not (i % 4)) puts("");
		printf("#%02d:{", i);
		printf(".m=%s, ", nodes[i].malicious ? magenta "bad " reset : "good");
		printf(".p=%02d, ", nodes[i].usage_pattern);
		printf(".u=%02d/%02d, ", nodes[i].total_time_transmitting, nodes[i].total_time_not_idle);
		if (nodes[i].on_band != -1) 
			printf(".b=%02d, ", nodes[i].on_band);
		else 
			printf(".b=  , ");
		
		if (nodes[i].time_remaining > 0) {
			printf(yellow); printf(".t=%02d, ", nodes[i].time_remaining); printf(reset);
		} else {
			printf(".t=  , ");
		}
		printf("s=");

		if (nodes[i].state == transmitting) printf(green);
		printf("[%02u]", i);
		if (nodes[i].state == transmitting) printf(reset);

		printf("}\t\t");
	}
	puts("");
}


static void print_node_values(const int printing_trust) {
	printf("printing trust/affinity of all nodes: \n");
	for (int i = 0; i < node_count; i++) {

		printf(yellow "node #%d" reset ":\t {", i);
		if (printing_trust & 1) printf("." cyan "trust" reset " =    [ ");
		if (printing_trust & 1) for (int j = 0; j < node_count; j++) {
			const double t = nodes[i].trust[j];
			if (t == 0.0) printf(blue);
			if (t < 0.00) printf(red);
			if (t > 0.95) printf(green);
			printf("%5.2lf ", t);
			if (t == 0.0) printf(reset);
			if (t < 0.00) printf(reset);
			if (t > 0.95) printf(reset);
		}

		printf("], \n\t\t  ");
		if (printing_trust & 2) printf("." green "affinity" reset " = [ ");
		if (printing_trust & 2) for (int j = 0; j < node_count; j++) {
			const double a = nodes[i].affinity[j];
			if (a == 0.0) printf(blue);
			if (a < 0.00) printf(red);
			if (a > 0.95) printf(green);
			printf("%5.2lf ", a);
			if (a == 0.0) printf(reset);
			if (a < 0.00) printf(reset);
			if (a > 0.95) printf(reset);
		}
		printf("] } \n");
	}
	printf("\n");
}

static void print_ints(int* array, int count) {
	printf("(%d){ ", count);
	for (int i = 0; i < count; i++) {
		printf("%d ", array[i]);
	}
	printf("}");
}

static void print_bands(void) {
	printf("printing bands:\n");
	for (int i = 0; i < band_count; i++) {
		if (bands[i].transmitter_count > congestion_threshold) printf(red);
		printf("\t#%d:{.freq=%05.3fHz,\t", i, bands[i].frequency);

		printf(yellow);
		printf(".listeners = ");
		print_ints(bands[i].listeners, bands[i].listener_count);
		printf(reset);

		printf(bands[i].transmitter_count > congestion_threshold ? red : not bands[i].transmitter_count ? blue : green);
		printf("  .transmitters = ");
		print_ints(bands[i].transmitters, bands[i].transmitter_count);
		printf(reset);

		puts("\n");
	}
	printf("[end of bands]\n");
}



/// ------------------------------------------- simulation functions ----------------------------------------


static int find_int(int* array, int count, int element) {
	for (int i = 0; i < count; i++) {
		if (array[i] == element) return i;
	}
	abort(); // return -1;
}

static void add_listener(const int b, const int node) {
	bands[b].listeners[bands[b].listener_count++] = node;
	nodes[node].on_band = b;
}

static void add_transmitter(const int b, const int node) {
	bands[b].transmitters[bands[b].transmitter_count++] = node;
	nodes[node].on_band = b;
}

static void remove_listener(const int e, const int at, const int i) {
	memmove(bands[e].listeners + at, bands[e].listeners + at + 1, (size_t) (bands[e].listener_count - at - 1) * sizeof(int));
	bands[e].listener_count--;
	nodes[i].on_band = -1;
}

static void remove_transmitter(const int e, const int at, const int i) {
	memmove(bands[e].transmitters + at, bands[e].transmitters + at + 1, (size_t) (bands[e].transmitter_count - at - 1) * sizeof(int));
	bands[e].transmitter_count--;
	nodes[i].on_band = -1;
}

static double neighbor_affinity(const struct node this, const struct band band) {
	double affinity_with_neighbors = 0.0;
	for (int j = 0; j < band.transmitter_count; j++) {
		affinity_with_neighbors += this.affinity[band.transmitters[j]];
	}
	return affinity_with_neighbors;
}

static double neighbor_trust(const struct node this, const struct band band) {
	double trust_with_neighbors = 0.0;
	for (int j = 0; j < band.transmitter_count; j++) {
		trust_with_neighbors += this.trust[band.transmitters[j]];
	}
	return trust_with_neighbors;
}


static void react_to_congestion(int b) {

	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			const double transmitter_i_time_remaining = (double) nodes[transmitter_i].time_remaining;
			const double transmitter_j_time_remaining = (double) nodes[transmitter_j].time_remaining;

			nodes[transmitter_i].affinity[transmitter_j] -= affinity_congestion_penalty * transmitter_j_time_remaining;
			nodes[transmitter_j].affinity[transmitter_i] -= affinity_congestion_penalty * transmitter_i_time_remaining;

			nodes[transmitter_i].trust[transmitter_j] -= trust_congestion_penalty * transmitter_j_time_remaining;
			nodes[transmitter_j].trust[transmitter_i] -= trust_congestion_penalty * transmitter_i_time_remaining;
		}
	}
}

static void nominally_increase_trust(int b) {
	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			nodes[transmitter_i].affinity[transmitter_j] += normal_affinity_boost;
			nodes[transmitter_j].affinity[transmitter_i] += normal_affinity_boost;

			nodes[transmitter_i].trust[transmitter_j] += normal_trust_boost;
			nodes[transmitter_j].trust[transmitter_i] += normal_trust_boost;
		}
	}
}

static void clip_trust_and_affinity(int i) {
	for (int j = 0; j < node_count; j++) {
		nodes[i].trust[j] = nodes[i].trust[j] > max_trust ? max_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] > max_affinity ? max_affinity : nodes[i].affinity[j];

		nodes[i].trust[j] = nodes[i].trust[j] < min_trust ? min_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] < min_affinity ? min_affinity : nodes[i].affinity[j];
	}
}


static void tick_clock_for_each_transmitter(int b) {
	for (int u = 0; u < bands[b].transmitter_count; u++) {

		const int transmitter = bands[b].transmitters[u];

		if (nodes[transmitter].time_remaining > 0)  nodes[transmitter].time_remaining--;
		else {
			nodes[transmitter].state = idle;
			nodes[transmitter].time_remaining = -1;
			remove_transmitter(b, u, transmitter); 
			u--;
		}
	}
}

static void simulate_timestep(void) {

	for (int b = 0; b < band_count; b++) {
		tick_clock_for_each_transmitter(b);
		if (bands[b].transmitter_count > congestion_threshold) react_to_congestion(b); 
		else nominally_increase_trust(b);
	}

	for (int i = 0; i < node_count; i++) {

		const struct node this = nodes[i];
		const int S = this.state;

		if (S == idle) {
			if (rand() % this.usage_pattern) goto next_node;
		listen_: 
			nodes[i].state = listening;
			add_listener(rand() % band_count, i);
			nodes[i].total_time_not_idle++;


		} else if (S == listening) {
			
			const int on = this.on_band;
			struct band band = bands[on];

			nodes[i].total_time_not_idle++;
			remove_listener(this.on_band, find_int(band.listeners, band.listener_count, i), i);

			if (band.transmitter_count > congestion_threshold and not this.malicious) goto listen_;
			nodes[i].state = transmitting;
			nodes[i].time_remaining = (rand() % this.max_transmit_time + minimum_transmit_time);
			add_transmitter(on, i);

		} else if (S == transmitting) {
			struct band band = bands[this.on_band];

			nodes[i].total_time_not_idle++;
			nodes[i].total_time_transmitting++;

			if (	this.malicious 							or
				neighbor_trust   (this, band) >= minimum_acceptable_trust      	and 
				neighbor_affinity(this, band) >= minimum_acceptable_affinity) 	goto next_node;

			remove_transmitter(this.on_band, find_int(band.transmitters, band.transmitter_count, i), i);
			goto listen_;
		}

		next_node: clip_trust_and_affinity(i);
	}
}





























/// ------------------------------------------- interrupt ----------------------------------------

static volatile bool running = true;
static volatile bool should_print_nodes = false;
static volatile bool should_print_bands = false;
static volatile int  should_print_values = 0;

static void handler(int __attribute__((unused))_) {

loop:	printf("entered interrupt handler:(q/b/n/t/a) ");
	fflush(stdout);
	const int c = getchar(); getchar();

	if (c == 'q') exit(0);

	else if (c == 'h') running = not running;
	else if (c == 'n') should_print_nodes = not should_print_nodes;
	else if (c == 'b') should_print_bands = not should_print_bands;

	else if (c == 't') should_print_values ^= 1;
	else if (c == 'a') should_print_values ^= 2;
	else {
		printf("unknown interrupt command: %c\n", c);
		goto loop;
	}
}












/// ------------------------------------------- recording data ----------------------------------------



static void compute_average_trust_affinity(
	double* out_mtrust, 
	double* out_trust,
	double* out_maff,
	double* out_aff
) {

	int mcount = 0;
	int count = 0;
	int fmcount = 0;
	int fcount = 0;

	double mtrust = 0.0;
	double trust = 0.0;
	double maff = 0.0;
	double aff = 0.0;

	for (int i = 0; i < node_count; i++) {
		if (nodes[i].malicious) {
			continue;
		}

		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				mtrust += nodes[i].trust[j];
				mcount++;
			} else {
				trust += nodes[i].trust[j];
				count++;
			}
		}

		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				maff += nodes[i].affinity[j];
				fmcount++;
			} else {
				aff += nodes[i].affinity[j];
				fcount++;
			}
		}
	}

	*out_mtrust = mtrust / mcount;
	*out_trust = trust / count;
	*out_maff = maff / fmcount;
	*out_aff = aff / fcount;
}






static void record_data(const int trial, const int ts) {
	if (not field_count) return;

	static double congestion = 0;
	static double emptiness = 0;

	for (int field = 0; field < field_count; field++) {

		const int F = recorded_data_fields[field];

		double average_transmitter_count = 0;
		double mtrust = 0, trust = 0, maff = 0, aff = 0;

		compute_average_trust_affinity(&mtrust, &trust, &maff, &aff);

		if (F == attr_congested_band_count) {
			double this_congestion = 0;
			for (int b = 0; b < band_count; b++) 
				if (bands[b].transmitter_count > congestion_threshold) 
					this_congestion++;

			if (not congestion) congestion = this_congestion;
			else congestion = (1.0 - congestion_weight) * congestion + congestion_weight * this_congestion; 
		}


		if (F == attr_empty_band_count) {
			double this_emptiness = 0;
			for (int b = 0; b < band_count; b++) 
				if (not bands[b].transmitter_count) this_emptiness++;

			if (not emptiness) emptiness = this_emptiness;
			else emptiness = (1.0 - congestion_weight) * emptiness + congestion_weight * this_emptiness; 
		}

		if (F == attr_average_transmitter_count) {
			double sum = 0;
			for (int b = 0; b < band_count; b++) sum += bands[b].transmitter_count;
			average_transmitter_count = sum / (double) band_count;
		}


		double value = 0;
		if (F == attr_congested_band_count) 		value = congestion;
		if (F == attr_empty_band_count) 		value = emptiness;
		if (F == attr_average_transmitter_count) 	value = average_transmitter_count;
		if (F == attr_average_mtrust) 			value = mtrust;
		if (F == attr_average_trust) 			value = trust;

		recorded_values[trial][ts][F] = value;
	}
	recorded_count++;
}

static void compute_average_data() {
	for (int ts = 0; ts < timestep_count; ts++) {
		for (int field = 0; field < field_count; field++) {
			double sum = 0;
			for (int trial = 0; trial < trial_count; trial++)   
				sum += recorded_data[trial][ts][field];
			average_values[ts][field] = sum / trial_count;
		}
	}
}

static void write_average_data(void) {
	if (not field_count) return;
	printf("writing the recorded data [%d, %d]...\n", field_count, timestep_count);

	FILE* file = fopen(output_file_name, "w+");
	if (not file) { perror("fopen"); exit(1); }


	fprintf(file, "timestep,");
	for (int field = 0; field < field_count; field++)  {
		fprintf(file, "%s", recordable_data_field_spelling[recorded_data_fields[field]]);
		if (field < field_count - 1) fprintf(file, ",");
	}
	fprintf(file, "\n");

	for (size_t ts = 0; ts < timestep_count; ts++) {
		fprintf(file, "%lu,", r);
		for (int field = 0; field < field_count; field++)  {
			fprintf(file, "%10.5lf", average_values[ts][field]);
			if (field < field_count - 1) fprintf(file, ",");
		}
		fprintf(file, "\n");
	}

	fclose(file);
}


static void allocate_record_arrays(void) {

	recorded_values = calloc(field_count, sizeof(double*));
	for (int i = 0; i < field_count; i++) recorded_values[i] = calloc(timestep_count, sizeof(double));

	node_recorded_values = calloc(node_field_count, sizeof(double**));
	for (int i = 0; i < node_field_count; i++) {
		node_recorded_values[i] = calloc(node_count / D, sizeof(double*));
		for (int m = 0; m < node_count / D; m++) {
			node_recorded_values[i][m] = calloc(timestep_count, sizeof(double));
		}
	}
}




static void initialize_nodes() {

	for (int i = 0; i < node_count; i++) {
		nodes[i].trust = calloc(node_count, sizeof(double));
		nodes[i].affinity = calloc(node_count, sizeof(double));
	}
	
	for (int i = 0; i < node_count; i++) {

		nodes[i].malicious = not (rand() % malicious_probability);
		nodes[i].state = idle;
		nodes[i].time_remaining = -1;
		nodes[i].on_band = -1;

		if (not nodes[i].malicious) {
			nodes[i].usage_pattern = rand() % 20 + 4;
			nodes[i].max_transmit_time = rand() % 10 + 4;
		} else {
			nodes[i].usage_pattern = rand() % 3 + 4;
			nodes[i].max_transmit_time = rand() % 100 + 4;
		}

		for (int j = 0; j < node_count; j++) {

			nodes[i].trust[j] =    (double)(rand() % 10000) / (double) 10000;
			nodes[i].affinity[j] = (double)(rand() % 10000) / (double) 10000;

			if (i == j) { nodes[j].affinity[i] = 0.00; }
			if (i == j) { nodes[j].trust[i]    = 0.00; }
		}
	}
}



static void initialize_bands() {
	for (int b = 0; b < band_count; b++) {
		bands[b].frequency = (double) (rand() % 45 + 45);
		bands[b].frequency *= 10;
		bands[b].transmitter_count = 0;
	}
}






/// ------------------------------------------- main ----------------------------------------



int main(void) {

	srand((unsigned) time(0));
	struct sigaction action = {.sa_handler = handler}; 
	sigaction(SIGINT, &action, NULL);


	for (int trial = 0; trial < trial_count; trial++) {

		printf("------------------- trial %d ------------------\n", trial);

		bands = calloc(band_count, sizeof(struct band));
		nodes = calloc(node_count, sizeof(struct node));

		initialize_bands();
		initialize_nodes();
		allocate_record_arrays();

		puts("simulating network...");

		for (int ts = 0; ts < timestep_count and running; ts++) {

			if (timestep_delay_us) clear_screen();

			if (should_print_values) 		print_node_values(should_print_values);
			if (should_print_nodes) 		print_nodes();
			if (should_print_bands) 		print_bands();
			
			simulate_timestep();
			

			if (timestep_delay_us) { fflush(stdout); usleep(timestep_delay_us); }

			record_data(trial, ts);
		}

		free(nodes);
		free(bands);
	

		double mtrust = 0.0, trust = 0.0, maff = 0.0, aff = 0.0;

		compute_average_trust_affinity(&mtrust, &trust, &maff, &aff);

		printf("mtrust = %lf, trust = %lf\n", mtrust, trust);
		printf("maffinity = %lf, affinity = %lf\n", maff, aff);
	}

	compute_average_data();
	write_average_data();
}

























































/*


collecting data:      find stastical difference between trust and mtrust values     using t test or 


	average and stdv  within one simulation  100000ts  (1mil???)        within 1 trial    look at variance

	10 simulations      look at variance.


			x axis is time      y axis aff and trust or      plot mtrust and plot trust        too!



					show convergence behavior   over time!


					

*/





























/*

double* affinity_sums = calloc(band_count, sizeof(double));

for (int b = 0; b < band_count; b++) {
	for (int j = 0; j < bands[b].transmitter_count; j++) {
		affinity_sums[b] += nodes[bands[b].transmitters[j]].affinity[i];
	}
}

int smallest_index = -1;
int smallest_transmitters = -1;
int largest_index = -1;
double largest_affinity = -999999;

for (int b = 0; b < band_count; b++) {

	if (bands[b].transmitter_count > smallest_transmitters) {
		smallest_index = b;
		smallest_transmitters = bands[b].transmitter_count;
	}

	if (affinity_sums[b] > largest_affinity) {
		largest_index = b;
		largest_affinity = affinity_sums[b];
	}
}

free(affinity_sums);

if (smallest_index < 0 or largest_index < 0) {
	printf("largest index was not initialized! aborting this attempt to transmit...\n");
	continue;
}



printf("after: "); print_ints(bands[this.on_band].transmitters, bands[this.on_band].transmitter_count); puts("");
				puts("");


*/







//for (int j = 0; j < node_count; j++) {
		//	// nodes[i].trust[j] = nodes[i].trust[j] + 0.05 * (0.5 - nodes[i].affinity[j]); // temporary
		//}


/*

puts("could not find node in array!");
				printf("band.transmitter_count = %d\n", band.transmitter_count);
				printf("b=%d: array: ", nodes[i].on_band); 
				print_ints(band.transmitters, band.transmitter_count); 
				puts("");
				printf("value = %d\n", i);
*/








/*

if (debug) { printf("after: "); print_ints(bands[b].transmitters, bands[b].transmitter_count); puts(""); puts(""); }




if (debug) printf("expiring %d from band %d...\n", bands[b].transmitters[i], b);
			if (debug) { printf("before: "); print_ints(bands[b].transmitters, bands[b].transmitter_count); puts(""); }





				if (this.time_remaining < 0) {
					puts("no time remaining, but tried to hop channels!...");
					abort();
				}

				if (debug) printf("removing %d from band %d...\n", band.transmitters[at], this.on_band);
				memmove(bands[this.on_band].transmitters + at, 
					bands[this.on_band].transmitters + at + 1, 
					(size_t) (bands[this.on_band].transmitter_count - at - 1) * sizeof(int));
				
				bands[this.on_band].transmitter_count--;

				int b = rand() % band_count;
				bands[b].transmitters[bands[b].transmitter_count++] = i;
				nodes[i].on_band = b;
				if (debug) printf("info: node #%d switched bands to %d...\n", i, b);



static void record_node_data(void) {
	if (not node_field_count) return;

	const int n = record_for_node_number;
	const int M = node_count / D;

	for (int i = 0; i < node_field_count; i++) {
		for (int m = 0; m < M; m++) {
			double value = 0;
			if (recorded_node_fields[i] == attr_trust) 		value = nodes[n].trust[m];
			if (recorded_node_fields[i] == attr_affinity) 		value = nodes[n].affinity[m];
			node_recorded_values[i][m][node_recorded_count] = value;
		}
	}
	node_recorded_count++;
}




static void write_recorded_node_data(void) {
	if (not node_field_count) return;
	const int M = node_count / D;
	printf("writing the recorded node data [%d, %d, %d]...\n", node_field_count, M, timestep_count);

	FILE* file = fopen(node_output_file_name, "w+");
	if (not file) {
		perror("fopen");
		exit(1);
	}

	fprintf(file, "timestep,");
	for (int i = 0; i < node_field_count; i++)  {
		for (int m = 0; m < M; m++) {
			fprintf(file, "%s_%d%s", recordable_data_field_spelling[recorded_node_fields[i]], m, nodes[m].malicious ? "M" : "");
			if (i < node_field_count - 1 or m < M - 1)  fprintf(file, ",");
		}
	}
	fprintf(file, "\n");

	for (size_t r = 0; r < recorded_count; r++) {
		fprintf(file, "%lu,", r);
		for (int i = 0; i < node_field_count; i++)  {
			for (int m = 0; m < M; m++) {
				fprintf(file, "%10.5lf", node_recorded_values[i][m][r]);
				if (i < node_field_count - 1 or m < M - 1) 
					fprintf(file, ",");
			}
		}
		fprintf(file, "\n");
	}
	fclose(file);
}

*/



