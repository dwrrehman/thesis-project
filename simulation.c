#include <stdio.h>   // written by dwrr on 202307311.202047: thesis research:
#include <stdlib.h>  // simulation of cognitive radio secondary transmitters on a network containing rogue nodes,
#include <string.h>  // trying to develop an algorithm/model which can detect rogue nodes using a trust model.
#include <unistd.h>
#include <iso646.h>
#include <stdbool.h> 
#include <time.h>
#include <math.h>
#include <signal.h>

enum node_state { idle, transmitting };

enum ffields {
	attr_utility,
	field_count
};

static const char* field_spelling[3 * field_count] = {
	"Utility.avg",
	"Utility.+std",
	"Utility.-std",
};

struct node {
	double p_idle_transmit;
	double p_transmit_idle;
	int state;
	int previous;
	int band;
	int malicious;
};

static const int band_count = 5;
static const int node_count = 30;
static const int trial_count = 10;
static const int timestep_count = 10000;
static const int decimation = 1;
static const int contention_threshold = 0;
static const double epsilon = 0.05;
static const double bandwidth = 20.0;
static const double background_gaussian_noise = 0.05;
static const double max_power = 4.0; 
static const double default_channel_gain = 1.0;
static const double switching_cost = 0.3 * bandwidth;
static const double initial_idle_transmit = 0.12;
static const double initial_transmit_idle = 0.20;
static const double malicious_probability = 0; 


static const char* const output_file_name = "output.csv";
static double recorded_values[trial_count][timestep_count][field_count] = {0};
static double average_values[timestep_count][field_count * 3] = {0};

static struct node* nodes = NULL;

static bool probability(double x) {
	const int r = rand() % 10000000;
	const double p = ((double) r) / 10000000.0; 
	return p < x;
}

static double R(int k) {
	const double signal = default_channel_gain * max_power;
	const double noise = background_gaussian_noise + ((double) k) * default_channel_gain * max_power;
	const double snr = signal / noise;
	const double reward = bandwidth * log2(1 + snr); 
	return reward;
}

static int K(const int b) {
	int count = 0;
	for (int i = 0; i < node_count; i++) {
		if (nodes[i].band == b and nodes[i].state == transmitting) {
			count++;
		}
	}
	return count;
}

static double W(void) {
	double sum = 0.0;
	for (int b = 0; b < band_count; b++) {
		const int k = K(b);
		sum += ((double) k) * R(k);
	}
	return sum;
}

static double C(void) {
	int count = 0;
	for (int i = 0; i < node_count; i++) {
		if (nodes[i].state == transmitting and nodes[i].previous == idle) {
			count++;
		}
	}
	return switching_cost * (double) count;
}

static double I(void) {
	return (W() - C()) / node_count;
}

static int pick_new_band(int i) {
	int b = nodes[i].band, x = b;
	while (x == b) x = rand() % band_count;
	return x;
}

static void simulate_timestep(void) {

	for (int i = 0; i < node_count; i++) {

		if (nodes[i].state == idle) {

			if (probability(nodes[i].p_idle_transmit)) {
				nodes[i].state = transmitting;
			} else {
				nodes[i].band = pick_new_band(i);
			}

			if (K(nodes[i].band) - nodes[i].state > contention_threshold) {
				nodes[i].p_idle_transmit -= epsilon;
				if (nodes[i].p_idle_transmit < 0) nodes[i].p_idle_transmit = 0;
			} else {
				nodes[i].p_idle_transmit += epsilon;
				if (nodes[i].p_idle_transmit > 1) nodes[i].p_idle_transmit = 1;
			}
			

		} else if (nodes[i].state == transmitting) {
		
			if (probability(nodes[i].p_transmit_idle)) {
				nodes[i].band = pick_new_band(i);
				nodes[i].state = idle;
			} 

			if (K(nodes[i].band) - nodes[i].state > contention_threshold) {
				nodes[i].p_transmit_idle += epsilon;
				if (nodes[i].p_transmit_idle > 1) nodes[i].p_transmit_idle = 1;
			} else {
				nodes[i].p_transmit_idle -= epsilon;
				if (nodes[i].p_transmit_idle < 0) nodes[i].p_transmit_idle = 0;
			}
		}
	}
}

static void compute_average_data(void) {

	for (int tr = 0; tr < trial_count; tr++) {
		double sum = 0.0;
		for (int i = 0; i < timestep_count; i++) {
			sum += recorded_values[tr][i][attr_utility];
			recorded_values[tr][i][attr_utility] = sum / (i + 1);
		}
	}
	
	for (int ts = 0; ts < timestep_count; ts++) {

		double sum = 0;
		for (int trial = 0; trial < trial_count; trial++) {
			sum += recorded_values[trial][ts][attr_utility];
		}
		const double average = sum / trial_count;
		
		double stdev_sum = 0;
		for (int trial = 0; trial < trial_count; trial++) {
			const double x = (recorded_values[trial][ts][attr_utility] - average);
			stdev_sum += x * x;
		}
		const double stdev = sqrt(stdev_sum / trial_count);

		average_values[ts][3 * attr_utility + 0] = average;
		average_values[ts][3 * attr_utility + 1] = average + stdev;
		average_values[ts][3 * attr_utility + 2] = average - stdev;
	}
}


static void write_average_data(void) {
	printf("writing the recorded data [%d, %d]...\n", field_count, timestep_count);

	FILE* file = fopen(output_file_name, "w+");
	if (not file) { perror("fopen"); exit(1); }

	fprintf(file, "TS(%d.%d.%d.%d), ", trial_count, timestep_count, node_count, band_count);
	for (int field = 0; field < 3 * field_count; field++)  {
		fprintf(file, "%s", field_spelling[field]);
		if (field < 3 * field_count - 1) fprintf(file, ", ");
	}
	fprintf(file, "\n");

	for (size_t ts = 0; ts < timestep_count; ts++) {
		if (ts % decimation) continue;
		fprintf(file, "%lu,", ts);
		for (int field = 0; field < 3 * field_count; field++)  {
			fprintf(file, "%10.5lf", average_values[ts][field]);
			if (field < 3 * field_count - 1) fprintf(file, ",");
		}
		fprintf(file, "\n");
	}

	fclose(file);
}

static void initialize_nodes(void) {
	nodes = calloc(node_count, sizeof(struct node));
	for (int i = 0; i < node_count; i++) {
		nodes[i].malicious = probability(malicious_probability);
		nodes[i].state = idle;
		nodes[i].previous = idle;
		nodes[i].band = rand() % band_count;
		nodes[i].p_idle_transmit = initial_idle_transmit;
		nodes[i].p_transmit_idle = initial_transmit_idle;
	}
}

int main(void) {
	srand((unsigned) time(0));
	for (int trial = 0; trial < trial_count; trial++) {
		printf("simulating trial %d...\n", trial);
		initialize_nodes();
		for (int ts = 0; ts < timestep_count; ts++) {
			
			recorded_values[trial][ts][attr_utility] = I();
			for (int i = 0; i < node_count; i++) nodes[i].previous = nodes[i].state;
			simulate_timestep();
		}
		printf("Average Utility =          %lf\n", recorded_values[trial][timestep_count - 1][attr_utility]);
		free(nodes);
	}
	compute_average_data();
	write_average_data();
}








































































































/*

		int data_ts = 1;
		for (int field = 0; field < field_count; field++) {
			const int F = recorded_fields[field];
			if (F == attr_average_utility_p) recorded_values[trial][0][field] = 0;
			if (F == attr_average_utility_a) recorded_values[trial][0][field] = 0;
			if (F == attr_average_utility_m) recorded_values[trial][0][field] = 0;
			if (F == attr_average_mutility_p) recorded_values[trial][0][field] = 0;
			if (F == attr_average_mutility_a) recorded_values[trial][0][field] = 0;
			if (F == attr_average_mutility_m) recorded_values[trial][0][field] = 0;
		}





*/


































































		//double mtrust = 0.0, trust = 0.0, maff = 0.0, aff = 0.0;
		//compute_average_trust_affinity(&mtrust, &trust, &maff, &aff);

		//printf("mtrust = %lf, trust = %lf\n", mtrust, trust);
		//printf("maffinity = %lf, affinity = %lf\n", maff, aff);




	//const int attr_average_utility_index = 0;
	//const int attr_average_mutility_index = 1;

	//double m_util = 0, n_util = 0;
	//for (int t = 0; t < timestep_count; t++) {
	//	m_util += average_values[t][attr_average_mutility_index];
	//	n_util += average_values[t][attr_average_utility_index];
	//}
	//m_util /= timestep_count;
	//n_util /= timestep_count;

	//double msum = 0, nsum = 0;
	//for (int t = 0; t < timestep_count; t++) {
	//	const double mdiff = average_values[t][attr_average_mutility_index] - m_util;
	//	const double ndiff = average_values[t][attr_average_utility_index] - n_util;
	//	msum += mdiff * mdiff;
	//	nsum += ndiff * ndiff;
	//}

	//msum /= timestep_count;
	//nsum /= timestep_count;
	//const double m_std = sqrt(msum);
	//const double n_std = sqrt(nsum);
	
	//printf("--------------- INFO --------------------\n");
	//puts("");
	//printf("   Average Malicious Node Utility: %5.4lf ± %5.4lf           (St.dev.=%5.4lf)\n", m_util, m_std, m_std);
	//printf("   Average    Normal Node Utility: %5.4lf ± %5.4lf           (St.dev.=%5.4lf)\n", n_util, n_std, n_std);
	//puts("");

	//getchar();
	//system("./plotter/run output.csv");




	//nodes[i].time_remaining = -1;




//nodes[i].max_transmit_time = rand() % 10 + 4;
		//nodes[i].max_transmit_time = rand() % 10 + 4;



/*static void add_listener(const int b, const int node) {
	bands[b].listeners[bands[b].listener_count++] = node;
	nodes[node].on_band = b;
}*/

/*static void remove_listener(const int e, const int at, const int i) {
	memmove(bands[e].listeners + at, bands[e].listeners + at + 1, (size_t) (bands[e].listener_count - at - 1) * sizeof(int));
	bands[e].listener_count--;
	nodes[i].on_band = -1;
}*/
			// nodes[i].total_time_not_idle++;
			// nodes[i].total_time_transmitting += 1.0 / bands[this.on_band].transmitter_count;


//join_band:
	//nodes[i].total_time_not_idle++;



/*		} else if (S == listening) {
			
			const int on = this.on_band;
			struct band band = bands[on];

			//nodes[i].total_time_not_idle++;
			remove_listener(this.on_band, find_int(band.listeners, band.listener_count, i), i);

			const double background_gaussian_noise = 0.05; // 50mw
			const double power = (double)nodes[i].power / 1000.0; // in watts

			const double signal = default_channel_gain * power;

			double noise = background_gaussian_noise;
			for (int m = 0; m < band.transmitter_count; m++) {
				if (i == band.transmitters[m]) continue;
				noise += default_channel_gain * ((double)nodes[band.transmitters[m]].power / 1000.0); 
			}
			const double snr = signal / noise;

			//printf("checking if snr=%lf is >= than snr_thr=%lf...\n", snr, minimum_snr);
			if (snr < minimum_snr and not this.malicious) goto listen_;
			
			nodes[i].state = transmitting;
			//if (nodes[i].time_remaining < 0) nodes[i].time_remaining = (rand() % this.max_transmit_time + minimum_transmit_time);
			add_transmitter(on, i);
*/

		// if () { should_stay_on_band = true; goto decide; }


			/*

			const double background_gaussian_noise = 0.05; 		// 50mw
			const double power = (double)(nodes[i].power) / 1000.0;
			const double signal = default_channel_gain * power;
			double noise = background_gaussian_noise;

			for (int m = 0; m < band.transmitter_count; m++) {
				if (i == band.transmitters[m]) continue;
				noise += default_channel_gain * nodes[band.transmitters[m]].power;
			}

			const double snr = signal / noise;

			bool should_stay_on_band = false;
			int state = idle;

			
			if (neighbor_trust < 0.2) { state = listening; should_stay_on_band = false; goto decide; }

						//if (snr < minimum_snr) { should_stay_on_band = false; goto decide; }
			*/

		//printf(yellow);
		//printf(".listeners = ");
		//print_ints(bands[i].listeners, bands[i].listener_count);
		//printf(reset);







//if (neighbor_trust < defer_trust_threshold) goto next_node;
			//if (neighbor_aff   > defer_affinity_threshold) goto next_node;
			//if (neighbor_aff   > (double)(rand() % 1000) / 1000.0) goto next_node;


/*


//if (bands[b].transmitter_count > congestion_threshold) react_to_congestion(b); else nominally_increase_trust(b);


// printf("band had %d transmitters.\n", bands[b].transmitter_count);

static void react_to_congestion(int b) {

	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			//const double transmitter_i_time_remaining = (double) nodes[transmitter_i].time_remaining;
			//const double transmitter_j_time_remaining = (double) nodes[transmitter_j].time_remaining;

			nodes[transmitter_i].trust[transmitter_j] -= trust_congestion_penalty * nodes[transmitter_i].affinity[transmitter_j];
			nodes[transmitter_j].trust[transmitter_i] -= trust_congestion_penalty * nodes[transmitter_i].affinity[transmitter_j];

			// printf("decreased trust between %d and %d nodes\n", transmitter_i, transmitter_j);
		}
	}
}

static void nominally_increase_trust(int b) {
	for (int i = 0; i < bands[b].transmitter_count; i++) {
		for (int j = 0; j < bands[b].transmitter_count; j++) {
			if (i == j) continue;

			const int transmitter_i = bands[b].transmitters[i];
			const int transmitter_j = bands[b].transmitters[j];

			nodes[transmitter_i].trust[transmitter_j] += normal_trust_boost;
			nodes[transmitter_j].trust[transmitter_i] += normal_trust_boost;

			printf("just boosted trust between %d and %d nodes\n", transmitter_i, transmitter_j);
			abort();
		}
	}
}






	the main idea:
		- affinity values are adjusted as follows:
			if you see a node on the same band as you, you decrease its affinity value, 
			if you don't see that node again, while transmitting, 
				then you increase your affinity with that node, 
					then, if you are able to transmit on the same band as that node without congestion, you increase your trust with that node. trust nominimally increases while transmitting with a given node.
					you decrease your trust with that node in proportion to your affinity with it. 




static double neighbor_affinity(const struct node this, const struct band band) {
	double affinity_with_neighbors = 0.0;
	for (int j = 0; j < band.transmitter_count; j++) {
		affinity_with_neighbors += this.affinity[band.transmitters[j]];
	}
	return affinity_with_neighbors;
}






//   Band1: [ t1 t5 ]
//   Band2: [ t2 ]
//   Band3: [ t3 ]


literature review: 

		look into manets  adhoc networks

		see if these ideas have been done yet.





good ideas:
------------------------

	- incoorperating reciprocity: general/targeted, using trust/affinity combination 
		to determine probability of being alturistic to a given node. 

	- hetretogenus systems: nodes which have different requirements for transmission amounts

	- dynamic populations: nodes coming and leaving the network

	- using no agregator





less good ideas:
------------------------

	- avoid transmitting with nodes with lower trust value

	- threshold on trust for rogue node detection



*/















































/*

how to calc   std dev:



	calculate subtractions of        given value, minus sum

	square all of these subtractions. 


	sum these squares all together. 

	divide this sum by timestep_count


	take the square root of the resultant division. 


	boom. therese the std dev.




*/










































































































































































































/*


collecting data:      find stastical difference between trust and mtrust values     using t test or 


	average and stdv  within one simulation  100000ts  (1mil???)        within 1 trial    look at variance

	10 simulations      look at variance.


			x axis is time      y axis aff and trust or      plot mtrust and plot trust        too!



					show convergence behavior   over time!


					

*/





























/*

double* affinity_sums = calloc(band_count, sizeof(double));

for (int b = 0; b < band_count; b++) {
	for (int j = 0; j < bands[b].transmitter_count; j++) {
		affinity_sums[b] += nodes[bands[b].transmitters[j]].affinity[i];
	}
}

int smallest_index = -1;
int smallest_transmitters = -1;
int largest_index = -1;
double largest_affinity = -999999;

for (int b = 0; b < band_count; b++) {

	if (bands[b].transmitter_count > smallest_transmitters) {
		smallest_index = b;
		smallest_transmitters = bands[b].transmitter_count;
	}

	if (affinity_sums[b] > largest_affinity) {
		largest_index = b;
		largest_affinity = affinity_sums[b];
	}
}

free(affinity_sums);

if (smallest_index < 0 or largest_index < 0) {
	printf("largest index was not initialized! aborting this attempt to transmit...\n");
	continue;
}



printf("after: "); print_ints(bands[this.on_band].transmitters, bands[this.on_band].transmitter_count); puts("");
				puts("");


*/







//for (int j = 0; j < node_count; j++) {
		//	// nodes[i].trust[j] = nodes[i].trust[j] + 0.05 * (0.5 - nodes[i].affinity[j]); // temporary
		//}


/*

puts("could not find node in array!");
				printf("band.transmitter_count = %d\n", band.transmitter_count);
				printf("b=%d: array: ", nodes[i].on_band); 
				print_ints(band.transmitters, band.transmitter_count); 
				puts("");
				printf("value = %d\n", i);
*/








/*

if (debug) { printf("after: "); print_ints(bands[b].transmitters, bands[b].transmitter_count); puts(""); puts(""); }




if (debug) printf("expiring %d from band %d...\n", bands[b].transmitters[i], b);
			if (debug) { printf("before: "); print_ints(bands[b].transmitters, bands[b].transmitter_count); puts(""); }





				if (this.time_remaining < 0) {
					puts("no time remaining, but tried to hop channels!...");
					abort();
				}

				if (debug) printf("removing %d from band %d...\n", band.transmitters[at], this.on_band);
				memmove(bands[this.on_band].transmitters + at, 
					bands[this.on_band].transmitters + at + 1, 
					(size_t) (bands[this.on_band].transmitter_count - at - 1) * sizeof(int));
				
				bands[this.on_band].transmitter_count--;

				int b = rand() % band_count;
				bands[b].transmitters[bands[b].transmitter_count++] = i;
				nodes[i].on_band = b;
				if (debug) printf("info: node #%d switched bands to %d...\n", i, b);



static void record_node_data(void) {
	if (not node_field_count) return;

	const int n = record_for_node_number;
	const int M = node_count / D;

	for (int i = 0; i < node_field_count; i++) {
		for (int m = 0; m < M; m++) {
			double value = 0;
			if (recorded_node_fields[i] == attr_trust) 		value = nodes[n].trust[m];
			if (recorded_node_fields[i] == attr_affinity) 		value = nodes[n].affinity[m];
			node_recorded_values[i][m][node_recorded_count] = value;
		}
	}
	node_recorded_count++;
}




static void write_recorded_node_data(void) {
	if (not node_field_count) return;
	const int M = node_count / D;
	printf("writing the recorded node data [%d, %d, %d]...\n", node_field_count, M, timestep_count);

	FILE* file = fopen(node_output_file_name, "w+");
	if (not file) {
		perror("fopen");
		exit(1);
	}

	fprintf(file, "timestep,");
	for (int i = 0; i < node_field_count; i++)  {
		for (int m = 0; m < M; m++) {
			fprintf(file, "%s_%d%s", recordable_data_field_spelling[recorded_node_fields[i]], m, nodes[m].malicious ? "M" : "");
			if (i < node_field_count - 1 or m < M - 1)  fprintf(file, ",");
		}
	}
	fprintf(file, "\n");

	for (size_t r = 0; r < recorded_count; r++) {
		fprintf(file, "%lu,", r);
		for (int i = 0; i < node_field_count; i++)  {
			for (int m = 0; m < M; m++) {
				fprintf(file, "%10.5lf", node_recorded_values[i][m][r]);
				if (i < node_field_count - 1 or m < M - 1) 
					fprintf(file, ",");
			}
		}
		fprintf(file, "\n");
	}
	fclose(file);
}



static void allocate_record_arrays(void) {

	recorded_values = calloc(field_count, sizeof(double*));
	for (int i = 0; i < field_count; i++) recorded_values[i] = calloc(timestep_count, sizeof(double));

	node_recorded_values = calloc(node_field_count, sizeof(double**));
	for (int i = 0; i < node_field_count; i++) {
		node_recorded_values[i] = calloc(node_count / D, sizeof(double*));
		for (int m = 0; m < node_count / D; m++) {
			node_recorded_values[i][m] = calloc(timestep_count, sizeof(double));
		}
	}
}





*/







/*
todo:
	- visualize how nodes group together with others-  groups in the neighborhood graph using  affinity values!!!

	x - differentiate trust and affinity more...


	x - redo the simulation to account for    LISTENING of a node.  (not always transmitting!)


*/



//static const double minimum_acceptable_affinity = 	0.00;
//static const double minimum_acceptable_trust = 	0.05;












/*
 - hiearchacla

- hertrogeneius ** 

		bands and devices are hetro

- task allocation systems





power transmission level

transmission requiredements how often to transmit

specific bands

*/










/*static void update_bands(void) {
	for (int b = 0; b < band_count; b++) {
		for (int u = 0; u < bands[b].transmitter_count; u++) {

			const int transmitter = bands[b].transmitters[u];

			if (5 > 0) {}// nodes[transmitter].time_remaining--;
			else {
				//nodes[transmitter].state = idle;
				//nodes[transmitter].time_remaining = -1;
				//remove_transmitter(b, u, transmitter); 
				u--;
			}
		}
	}
}*/


			//printf("checking if snr=%lf is >= than snr_thr=%lf...\n", snr, minimum_snr);








			/*

if () goto next_node;
		listen_:
			nodes[i].state = listening;
			add_listener(rand() % band_count, i);
			nodes[i].total_time_not_idle++;





				theres a bug here:
		
				when we defer, we need to NOT generate a new timer set to ZEROOO


				we need to keep the old value of our transmission timer, and just pick up where we left off, but on a new band. 

					that way, we won't let nodes get more utility. 


						we'll solve this. 

			*/




		// dont_defer:

			/*for (int m = 0; m < band.transmitter_count; m++) {
				if (i == band.transmitters[m]) continue;
				nodes[i].trust[band.transmitters[m]] -= trust_congestion_penalty * nodes[i].affinity[band.transmitters[m]];
			}*/









// static const int minimum_transmit_time = 10;            // baseline amount of time to transmit on the network.








// todo: when doing distance introduce   rayleigh flat fading  for  channel gain.
// static const double minimum_snr = 7.0; // in dB






//static const double affinity_contention_increase = 	0.8;
//static const double normal_affinity_decrease = 		0.02;
//static const double trust_contention_penalty = 		0.12;
//static const double normal_trust_boost = 		0.0200;








//static void clear_screen(void) { printf("\033[H\033[2J"); }

/*

static void print_nodes(void) {
	printf("printing state of all nodes: \n");
	for (int i = 0; i < node_count; i++) {
		if (i and not (i % 4)) puts("");
		printf("#%02d:{", i);
		printf(".m=%s, ", nodes[i].malicious ? magenta "bad " reset : "good");
		if (nodes[i].on_band != -1)  printf(".b=%02d, ", nodes[i].on_band); else  printf(".b=  , ");
		printf(yellow); printf(".t=%02d, ", 99); printf(reset);
		printf("s=");
		if (nodes[i].state == transmitting) printf(green);
		printf("[%02u]", i);
		if (nodes[i].state == transmitting) printf(reset);

		printf("}\t\t");
	}
	puts("");
}

static void print_node_values(const int printing_trust) {
	printf("printing trust/affinity of all nodes: \n");
	for (int i = 0; i < node_count; i++) {

		printf(yellow "node #%d" reset ":\t {", i);
		if (printing_trust & 1) printf("." cyan "trust" reset " =    [ ");
		if (printing_trust & 1) for (int j = 0; j < node_count; j++) {
			const double t = nodes[i].trust[j];
			if (t == 0.0) printf(blue);
			if (t < 0.00) printf(red);
			if (t > 0.95) printf(green);
			printf("%5.2lf ", t);
			if (t == 0.0) printf(reset);
			if (t < 0.00) printf(reset);
			if (t > 0.95) printf(reset);
		}

		printf("], \n\t\t  ");
		if (printing_trust & 2) printf("." green "affinity" reset " = [ ");
		if (printing_trust & 2) for (int j = 0; j < node_count; j++) {
			const double a = nodes[i].affinity[j];
			if (a == 0.0) printf(blue);
			if (a < 0.00) printf(red);
			if (a > 0.95) printf(green);
			printf("%5.2lf ", a);
			if (a == 0.0) printf(reset);
			if (a < 0.00) printf(reset);
			if (a > 0.95) printf(reset);
		}
		printf("] } \n");
	}
	printf("\n");
}

static void print_ints(int* array, int count) {
	printf("(%d){ ", count);
	for (int i = 0; i < count; i++) {
		printf("%d ", array[i]);
	}
	printf("}");
}

static void print_bands(void) {
	printf("printing bands:\n");
	for (int i = 0; i < band_count; i++) {
		if (bands[i].transmitter_count > contention_threshold) printf(red);
		printf("\t#%d:{.freq=%05.3fHz,\t", i, bands[i].frequency);
		printf(bands[i].transmitter_count > contention_threshold ? red : not bands[i].transmitter_count ? blue : green);
		printf("  .transmitters = ");
		print_ints(bands[i].transmitters, bands[i].transmitter_count);
		printf(reset);

		puts("\n");
	}
	printf("[end of bands]\n");
}





static void remove_transmitter(const int e, const int at, const int i) {
	memmove(bands[e].transmitters + at, bands[e].transmitters + at + 1, (size_t) (bands[e].transmitter_count - at - 1) * sizeof(int));
	bands[e].transmitter_count--;
	nodes[i].on_band = -1;
}


*/

//bands[b].transmitters[bands[b].transmitter_count++] = node;





//static void add_transmitter(const int b, const int node) {
//	nodes[node].on_band = b;
//}







/*static int get_neighbor_values(double* trust, double* affinity, int this) {
	int count = 0;
	for (int j = 0; j < node_count; j++) {
		if (this == j) continue;
		if (nodes[j].on_band == nodes[this].on_band and nodes[j].state == transmitting) {
			*affinity += nodes[this].affinity[j];
			*trust += nodes[this].trust[j];
			count++;
		}
	}
	
	if (count) *trust /= count;
	if (count) *affinity /= count;
	return count;
}










static void clip_trust_and_affinity(int i) {
	for (int j = 0; j < node_count; j++) {
		nodes[i].trust[j] = nodes[i].trust[j] > max_trust ? max_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] > max_affinity ? max_affinity : nodes[i].affinity[j];

		nodes[i].trust[j] = nodes[i].trust[j] < min_trust ? min_trust : nodes[i].trust[j];
		nodes[i].affinity[j] = nodes[i].affinity[j] < min_affinity ? min_affinity : nodes[i].affinity[j];
	}
}




*/

			/*for (int j = 0; j < node_count; j++) {
				if (i == j) continue;
				nodes[i].affinity[j] -= normal_affinity_decrease;
			}

			for (int m = 0; m < band.transmitter_count; m++) {
				if (i == band.transmitters[m]) continue;
				nodes[i].affinity[band.transmitters[m]] += affinity_contention_increase;
			}

			for (int j = 0; j < node_count; j++) {
				if (i == j) continue;
				nodes[i].trust[j] += normal_trust_boost * nodes[i].affinity[j];
			}*/



			//double neighbor_trust = 0, neighbor_aff = 0;
			//const int neighbor_count = get_neighbor_values(&neighbor_trust, &neighbor_aff, i);
	
			//if (not neighbor_count) goto probabalistically_stay;  //  this.malicious or 
			//if (neighbor_trust < 0.2) { should_stay_on_band = false; goto decide; }




/*for (int m = 0; m < band.transmitter_count; m++) {
					if (i == band.transmitters[m]) continue;
					nodes[i].trust[band.transmitters[m]] -= 
						trust_contention_penalty * 
						nodes[i].affinity[band.transmitters[m]];
				}*/


/*

static void handler(int __attribute__((unused))_) {

loop:	printf("entered interrupt handler:(q/b/n/t/a) ");
	fflush(stdout);
	const int c = getchar(); getchar();

	if (c == 'q') exit(0);

	else if (c == 'h') running = not running;
	else if (c == 'n') should_print_nodes = not should_print_nodes;
	else if (c == 'b') should_print_bands = not should_print_bands;

	else if (c == 't') should_print_values ^= 1;
	else if (c == 'a') should_print_values ^= 2;
	else {
		printf("unknown interrupt command: %c\n", c);
		goto loop;
	}
}



static void initialize_bands(void) {
	for (int b = 0; b < band_count; b++) {
		bands[b].frequency = (double) (rand() % 45 + 45);
		bands[b].frequency *= 10;
		bands[b].transmitter_count = 0;
	}
}




double* trust;
	double* affinity;






//struct sigaction action = {.sa_handler = handler}; 
	//sigaction(SIGINT, &action, NULL);






if (timestep_delay_us) clear_screen();

			if (should_print_values) 	print_node_values(should_print_values);
			if (should_print_nodes) 	print_nodes();
			if (should_print_bands) 	print_bands();
			
if (timestep_delay_us) { fflush(stdout); usleep(timestep_delay_us); }







static void compute_average_trust_affinity(
	double* out_mtrust, 
	double* out_trust,
	double* out_maff,
	double* out_aff
) {
	int mcount = 0, count = 0, fmcount = 0, fcount = 0;
	double mtrust = 0.0, trust = 0.0;
	double maff = 0.0, aff = 0.0;

	for (int i = 0; i < node_count; i++) {
		if (nodes[i].malicious) continue;

		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				mtrust += nodes[i].trust[j];
				mcount++;
			} else {
				trust += nodes[i].trust[j];
				count++;
			}
		}
		for (int j = 0; j < node_count; j++) {
			if (i == j) continue;
			if (nodes[j].malicious) {
				maff += nodes[i].affinity[j];
				fmcount++;
			} else {
				aff += nodes[i].affinity[j];
				fcount++;
			}
		}
	}
	*out_mtrust = mtrust / (mcount + 1);
	*out_trust = trust / (count + 1);
	*out_maff = maff / (fmcount + 1);
	*out_aff = aff / (fcount + 1);
}







*/






		//double 	average_transmitter_count = 0, mtrust = 0, trust = 0, maffinity = 0, affinity = 0;

		//compute_average_trust_affinity(&mtrust, &trust, &maffinity, &affinity);

		/*if (F == attr_congested_band_count) {
			double this_congestion = 0;
			for (int b = 0; b < band_count; b++) 
				if (bands[b].transmitter_count > contention_threshold) 
					this_congestion++;

			congestion = this_congestion;
			// if (not congestion)  else congestion = (1.0 - weighting) * congestion + weighting * this_congestion; 
		}

		if (F == attr_empty_band_count) {
			double this_emptiness = 0;
			for (int b = 0; b < band_count; b++) 
				if (not bands[b].transmitter_count) this_emptiness++;

			emptiness = this_emptiness;
			// if (not emptiness)    else emptiness = (1.0 - weighting) * emptiness + weighting * this_emptiness; 
		}

		if (F == attr_average_transmitter_count) {
			double sum = 0;
			for (int b = 0; b < band_count; b++) sum += bands[b].transmitter_count;
			average_transmitter_count = sum / (double) band_count;
		}*/



		//if (F == attr_congested_band_count) 		value = congestion;
		//if (F == attr_empty_band_count) 		value = emptiness;
		//if (F == attr_average_transmitter_count) 	value = average_transmitter_count;

/*
		if (F == attr_average_mtrust_p) 		value = mtrust;
		if (F == attr_average_mtrust_a) 		value = mtrust;
		if (F == attr_average_mtrust_m) 		value = mtrust;
		if (F == attr_average_trust_p) 			value = trust;
		if (F == attr_average_trust_a) 			value = trust;
		if (F == attr_average_trust_m) 			value = trust;

		if (F == attr_average_maffinity_p) 		value = maffinity;
		if (F == attr_average_maffinity_a) 		value = maffinity;
		if (F == attr_average_maffinity_m) 		value = maffinity;
		if (F == attr_average_affinity_p) 		value = affinity;
		if (F == attr_average_affinity_a) 		value = affinity;
		if (F == attr_average_affinity_m) 		value = affinity;







if (F == attr_average_usage) 			value = compute_usage(0);
		if (F == attr_average_musage) 			value = compute_usage(1);




static double compute_usage(bool m) {
	double result = 0.0;
	int count = 0;
	for (int i = 0; i < node_count; i++) {
		// if (not nodes[i].total_time_not_idle) continue;
		if (nodes[i].malicious xor m) continue;
		//const double ratio = nodes[i].total_time_transmitting / (double) nodes[i].total_time_not_idle;
		//result += ratio;
		count++;
	}
	if (count) result /= count; else result = 0;
	return result;
}








*/




// printf("%10.10lf  -- %10.10lf\n", average, result);




	//for (int b = 0; b < band_count; b++) {

		//const struct band band = bands[b];

		//if (not band.transmitter_count) continue;
	/*if (snr > 1 or snr < 0) {
			//printf("ERROR: ERROR : ERROR: \n");
			//abort();
			
		}*/
		// printf("R    %10.10lf  -- %10.10lf\n", result, reward);	
	//} 








/*	attr_average_trust_p,
	attr_average_trust_a,
	attr_average_trust_m,
	attr_average_mtrust_p,
	attr_average_mtrust_a,
	attr_average_mtrust_m,

	attr_average_affinity_p,
	attr_average_affinity_a,
	attr_average_affinity_m,
	attr_average_maffinity_p,
	attr_average_maffinity_a,
	attr_average_maffinity_m, 







	attr_average_mtrust_p,
	attr_average_mtrust_a,
	attr_average_mtrust_m,
	attr_average_trust_p,
	attr_average_trust_a,
	attr_average_trust_m,

	attr_average_maffinity_p,
	attr_average_maffinity_a,
	attr_average_maffinity_m,
	attr_average_affinity_p,
	attr_average_affinity_a,
	attr_average_affinity_m,







	"Average.Malicious.Trust+stdev",
	"Average.Malicious.Trust+0",
	"Average.Malicious.Trust-stdev",	
	"Average.Normal.Trust+stdev",
	"Average.Normal.Trust+0",
	"Average.Normal.Trust-stdev",

	"Average.Malicious.Affinity+stdev",
	"Average.Malicious.Affinity+0",
	"Average.Malicious.Affinity-stdev",	
	"Average.Normal.Affinity+stdev",
	"Average.Normal.Affinity+0",
	"Average.Normal.Affinity-stdev",



static volatile bool running = 1;
static volatile bool should_print_nodes = 0;
static volatile bool should_print_bands = 0;
static volatile int should_print_values = 0;


static const double max_trust = 	 1.0;
static const double max_affinity = 	 1.0;
static const double min_trust = 	 0.0;
static const double min_affinity = 	 0.0;



#define red   	"\x1B[31m"
#define green   "\x1B[32m"
#define yellow  "\x1B[33m"
#define blue   	"\x1B[34m"
#define magenta "\x1B[35m"
#define cyan   	"\x1B[36m"
#define reset 	"\x1B[0m"



static int find_int(int* array, int count, int element) {
	for (int i = 0; i < count; i++) {
		if (array[i] == element) return i;
	}
	abort(); 
}





*/





	//for (int i = 0; i < node_count; i++) {
		//nodes[i].trust = calloc(node_count, sizeof(double));
		//nodes[i].affinity = calloc(node_count, sizeof(double));
	//}

		/*for (int j = 0; j < node_count; j++) {
			nodes[i].trust[j] =    (double)(rand() % 10000) / (double) 10000;
			nodes[i].affinity[j] = (double)(rand() % 10000) / (double) 10000;
			if (i == j) { nodes[j].affinity[i] = 0.00; }
			if (i == j) { nodes[j].trust[i]    = 0.00; }
		}*/




