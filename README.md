# Thesis Stuff : Simulation Code for the Bio-Inspired Cognitive Radio Project

This is the simulation code for the thesis work done by Daniel Rehman, Malcolm Anderson, and Truc Duong, with our thesis advisor, Anna Wisniewska.

Currently the code is still a work in progress. 

This file will be updated with instructions on how to run the simulations once they are more complete.

## Building the source:

To build the project, you can simply compile the simulation.c program with:

```
gcc simulation.c -o executablenamehere -O3
```

or if you have the `clang` compiler toolchain installed on your system, simply use the included `build` script, and build in release mode:

```
./build release
```

